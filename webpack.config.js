const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var lessLoader = new ExtractTextPlugin('styles.css');
var cssLoader = new ExtractTextPlugin('index.css');

// const InlineEnviromentVariablesPlugin = require('inline-environment-variables-webpack-plugin');

module.exports = {
  devtool: 'source-map',
  entry: {},
  module: {
    //  preLoaders: [{test: /\.js$/, exclude: [/app\/lib/, /node_modules/], loader: 'eslint?{fix:false}'}],

    loaders: [
      {
        test: /\.js$/,
        exclude: [/app\/lib/, /node_modules/, /app\/assets/],
        loader: 'ng-annotate!babel!required?import[]=angular',
      },
      {test: /\.html$/, exclude: /node_modules/, loader: 'raw'},
      {
        test: /\.htm$/,
        exclude: /node_modules/,
        loader: 'ng-cache?prefix=[dir]/[dir]',
      },

      {
        test: /\.woff/,
        loader: require.resolve('url-loader') +
          '?prefix=font/&limit=10000&mimetype=application/font-woff&name=assets/[hash].[ext]',
      },
      {
        test: /\.ttf/,
        loader: require.resolve('file-loader') + '?prefix=font/&name=assets/[hash].[ext]',
      },
      {
        test: /\.eot/,
        loader: require.resolve('file-loader') + '?prefix=font/&name=assets/[hash].[ext]',
      },
      {
        test: /\.svg/,
        loader: require.resolve('file-loader') + '?prefix=font/&name=assets/[hash].[ext]',
      },
      // {test: /\.css$/, loader: ExtractTextPlugin.extract('style', 'css')},
      // {test: /\.less$/, loader: ExtractTextPlugin.extract('style', 'css!less')},

      // {test: /\.css$/, loader: 'style?insertAt=top!css?sourceMap'},
      {test: /\.css$/, loader: cssLoader.extract('css?sourceMap')},

      // {test: /\.less$/, loader: 'style?singleton!css?sourceMap!less?sourceMap'},
      {test: /\.less$/, loader: lessLoader.extract('css?sourceMap!less?sourceMap')},
    ],
  },
  plugins: [
    // Injects bundles in your index.html instead of wiring all manually.
    // It also adds hash to all injected assets so we don't have problems
    // with cache purging during deployment.
    new HtmlWebpackPlugin({
      template: 'client/index.html',
      inject: 'body',
      hash: true,
    }),

    // Automatically move all modules defined outside of application directory to vendor bundle.
    // If you are using more complicated project structure, consider to specify common chunks manually.
    new webpack.optimize.CommonsChunkPlugin({
      name: 'vendor',
      minChunks: function(module, count) {
        return module.resource && module.resource.indexOf(path.resolve(__dirname, 'client')) === -1;
      },
    }),
    lessLoader,
    cssLoader,
  ],
};
