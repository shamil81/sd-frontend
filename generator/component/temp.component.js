import template from './<%= name %>.html';
import controller from './<%= name %>.controller';
import './<%= name %>.less';

const <%= name %>Component = {
  bindings: {},
  template,
  controller,
};

export default <%= name %>Component;
