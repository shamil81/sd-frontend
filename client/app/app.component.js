import 'angular-material/angular-material.css';
import './material.less';
import template from './app.html';
import controller from './app.controller';
import './app.less';

const appComponent = {
  template,
  controller,
};

export default appComponent;
