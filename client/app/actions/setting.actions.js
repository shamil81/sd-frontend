import angular from 'angular';
import SETTING from '../constants/setting';
import {normalize, schema} from 'normalizr';

import {addHours, addDays, setMinutes, getMinutes, getTime, isAfter} from 'date-fns';

class SettingActions {
  constructor($ngRedux, $api) {
    this.$ngRedux = $ngRedux;
    this.$api = $api;
  }

  setSetting(payload) {
    return {
      type: SETTING.SUCCESS,
      payload,
    };
  }
  roundMinutes(val) {
    return 15 * Math.round(val / 15);
  }

  getDates(settings) {
    let d = new Date();
    const minutes = this.roundMinutes(getMinutes(d));
    setMinutes(d, minutes);
    const minDiff = settings.minBookingHours / 24;

    console.dir(settings);

    let minStart = new Date(settings.minStart);

    if (isAfter(d, minStart)) {
      minStart = d;
    }

    console.dir(minStart);
    const minEnd = addHours(minStart, settings.minBookingHours);
    const maxStart = addHours(minStart, settings.maxBookingHours);

    const maxEnd = addHours(minEnd, settings.maxBookingHours);

    return {minStart, maxStart, minEnd, maxEnd, minDiff};
  }

  load(city) {
    return () => {
      console.log(city);
      return this.$api.fetch('booking/setting', {city}).then(settings => {
        const defaults = this.getDates(settings);
        this.$ngRedux.dispatch(this.setSetting(defaults));
      });
    };
  }
}
SettingActions.$inject = ['$ngRedux', '$api'];
export default angular.module('SettingActions', []).service('$setting', SettingActions).name;
