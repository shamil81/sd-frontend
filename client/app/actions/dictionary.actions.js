import angular from 'angular';
import DICTIONARY from '../constants/dictionary';
import {normalize, schema} from 'normalizr';

class DictionaryActions {
  constructor($api, $ngRedux, $q, $setting) {
    this._http = $api;
    this.$ngRedux = $ngRedux;
    this._q = $q;
  }

  load() {
    const resources = [
      'city',
      'carmaker',
      'carcolor',
      'carbodystyle',
      'carmodel',
      'priceoptiongroup',
      'priceplan',
      'rentconditiongroup',
      'fueltype',
      'customerrequirement',
      'wheeldrive',
      'carclass',
    ];

    const fns = resources.map(resource => {
      return this._http.fetch(resource);
    });

    return dispatch => {
      return this._q
        .all(fns)
        .then(results => {
          const dictionary = {};
          resources.forEach((resource, i) => {
            const s = new schema.Entity(resource);
            const schemaList = [s];
            dictionary[resource] = normalize(results[i], schemaList).entities[resource];
          });
          dispatch({
            type: DICTIONARY.LOADED,
            payload: dictionary,
          });
        })
        .catch(error => {
          dispatch({
            type: DICTIONARY.ERROR,
            payload: error,
            error: true,
          });
        });
    };
  }
}
DictionaryActions.$inject = ['$api', '$ngRedux', '$q', '$setting'];

export default angular.module('DictionaryActions', []).service('$dictionary', DictionaryActions).name;
