import angular from 'angular';
import BOOKING from '../constants/booking';

class BookingActions {
  constructor($api, $ngRedux, $q) {
    this.$ngRedux = $ngRedux;
    this.$api = $api;
    this.$q = $q;
  }

  setStart(date) {
    return {
      type: BOOKING.SET_START,
      payload: {
        start: +date.start,
      },
    };
  }

  setEnd(date) {
    return {
      type: BOOKING.SET_END,
      payload: {
        end: +date.end,
      },
    };
  }

  setDates(date) {
    return {
      type: BOOKING.SET_DATES,
      payload: {
        start: +date.start,
        end: +date.end,
      },
    };
  }

  setCar(payload) {
    // this.$ngRedux.dispatch(this.getPriceOptions(result.owner));

    return {
      type: BOOKING.SET_CAR,
      payload,
    };
  }

  requestError(err) {
    return {
      type: BOOKING.CAR_REQUEST_ERROR,
      payload: err,
    };
  }

  getCar(p) {
    return () => {
      this.$api
        .fetch('car', p)
        .then(result => {
          this.$ngRedux.dispatch(this.setCar(result));
        })
        .catch(err => {
          this.$ngRedux.dispatch(
            (() => {
              return {
                type: BOOKING.ERROR,
                payload: err,
              };
            })(),
          );
        });
    };
  }

  createIntent(p) {
    return () => {
      this.$api
        .create('booking', {car: p.id, start: p.start, end: p.end})
        .then(results => {
          this.$ngRedux.dispatch(
            (() => {
              return {
                type: BOOKING.INTENT_SUCCESS,
                payload: results,
              };
            })(),
          );
        })
        .catch(err => {
          this.$ngRedux.dispatch(
            (() => {
              return {
                type: BOOKING.INTENT_ERROR,
                payload: err,
              };
            })(),
          );
        });
    };
  }

  cancel() {
    return () => {
      this.$ngRedux.dispatch(
        (() => {
          return {
            type: BOOKING.CANCEL_REQUEST,
          };
        })(),
      );

      const booking = this.$ngRedux.getState().booking;

      this.$api
        // .destroy(`booking?id=${booking.id}`)
        .destroy('booking')
        .then(id => {
          this.$ngRedux.dispatch(
            (() => {
              return {
                type: BOOKING.CANCEL,
              };
            })(),
          );
        })
        .catch(err => {
          this.$ngRedux.dispatch(
            (() => {
              return {
                type: BOOKING.CANCEL_ERROR,
                payload: err,
                error: true,
              };
            })(),
          );
        });
    };
  }

  create() {
    return () => {
      const booking = this.$ngRedux.getState().booking;

      let payload = {
        car: booking.car.id,
        id: booking.id,
        priceoptions: booking.priceoptions.map(function(val) {
          return val.id;
        }),
        start: booking.start,
        end: booking.end,
      };

      this.$api
        .create('booking', payload)
        .then(results => {
          return {
            type: BOOKING.SUCCESS,
            payload: results.id,
          };
        })
        .catch(err => {
          return {
            type: BOOKING.ERROR,
            payload: err,
          };
        });
    };
  }

  getPriceOptions(owner) {
    return () => {
      this.$api.fetch('priceoption', {createdBy: owner}).then(priceOptions => {
        this.$ngRedux.dispatch(
          (() => {
            return {
              type: BOOKING.PRICE_OPTIONS,
              payload: priceOptions,
            };
          })(),
        );

        // Create booking intent
        this.$ngRedux.dispatch(this.createIntent());
      });
    };
  }

  addPriceOption(payload) {
    return {
      type: BOOKING.ADD_PRICE_OPTION,
      payload,
    };
  }

  removePriceOption(payload) {
    return {
      type: BOOKING.REMOVE_PRICE_OPTION,
      payload,
    };
  }

  getCarImages(id) {
    return () => {
      this.$api
        .fetch('car/image/{$id}')
        .then(images => {
          this.$ngRedux.dispatch(
            (() => {
              return {
                type: BOOKING.IMAGES,
                payload: images,
              };
            })(),
          );
        })
        .catch(console.error);
    };
  }

  getModelInfo(id) {
    return () => {};
  }

  // getCar(id, start, end) {
  //   return () => {
  //     this.$api
  //       .fetch('car', {id, start, end})
  //       .then(result => {
  //         this.$ngRedux.dispatch(this.setCar(result));
  //       })
  //       .catch(err => {
  //         this.$ngRedux.dispatch(this.requestError(err));
  //       });
  //   };
  // }
}

BookingActions.$inject = ['$api', '$ngRedux', '$q'];

export default angular.module('BookingActions', []).service('$booking', BookingActions).name;
