describe('GeopositionService', () => {

  let GeopositionService, $rootScope, $q, val, $httpBackend;

  const cities = [
    {id: 1, isDefault: false},
    {id: 2, isDefault: true}
  ];

  const urlParams = {

    lat: 1,
    lng: 2

  };

  const positionResponse = {coords: {accuracy: 20, latitude: 1, longitude: 2}};


  beforeEach(() => {

    angular.mock.module('app.location');
    inject((_$geoposition_, _$rootScope_, _$q_) => {

      $rootScope = _$rootScope_;
      GeopositionService = _$geoposition_;
      $q = _$q_;


    });



  });


  it('should be defined', () => {
    expect(GeopositionService).toBeDefined();
  });

  it('should return the default city', () => {
    expect(GeopositionService.defaultCity(cities)).toEqual(2);
  });

  it('should return geolocation object from the coordinates passed as a parameter', () => {

    GeopositionService.getPosition(urlParams)
        .then(value => {
          val = value;
        });
    $rootScope.$digest();

    expect(val).toEqual(positionResponse);

  });

  it('should use navigator\'s geolocation api', () => {


    spyOn(navigator.geolocation, 'getCurrentPosition');

    GeopositionService.getPosition()
        .then(value => {

          val = value;

        });

    $rootScope.$digest();

    expect(navigator.geolocation.getCurrentPosition).toHaveBeenCalled();

  });




  describe('GeopositionService Init', () => {

    beforeEach(inject((_$httpBackend_) => {
      $httpBackend = _$httpBackend_;

      $httpBackend.whenGET(/assets.*/).respond(200, '');
      $httpBackend.whenGET(/api.*/).respond(200, '');

      spyOn(GeopositionService, 'getPosition').and.returnValue($q.when({
        coords: {
          latitude: 50.345,
          longitude: 39.123,
          accuracy: 20
        }
      }));

      spyOn(GeopositionService, 'getCity').and.returnValue($q.when(2));

    }));

    it('should return only a city', () => {
      GeopositionService.get({city: 2}, cities)
          .then(value => {
            val = value;
          });
      $rootScope.$digest();

      expect(val).toEqual({city: 2});

    });

    it('should return both city and position', () => {

      GeopositionService.get({city: 123}, cities)
          .then(value => {
            val = value;
          });

      $rootScope.$digest();

      expect(GeopositionService.getPosition).toHaveBeenCalled();

      expect(GeopositionService.getCity).toHaveBeenCalled();

      expect(val).toEqual({position: {lat: 50.345, lng: 39.123, accuracy: 20}, city: 2});

    });

  });

});