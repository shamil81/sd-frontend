import angular from 'angular';
import {stateGo, stateReload, stateTransitionTo} from 'redux-ui-router';

class AppActions {
  constructor($api, $ngRedux, $q, $setting, $geoposition, $dictionary, $state) {
    this.http = $api;
    this.$ngRedux = $ngRedux;
    this.$q = $q;
    this.$geoposition = $geoposition;
    this.$setting = $setting;
    this.$dictionary = $dictionary;
    this.$state = $state;
  }

  init($stateParams) {
    return () => {
      return this.$q((resolve, reject) => {
        this.$ngRedux.dispatch(this.$dictionary.load()).then(() => {
          this.$ngRedux
            .dispatch(
              this.$geoposition.get({
                cities: this.$ngRedux.getState().dictionary.city,
                urlParams: this.$ngRedux.getState().router.currentParams,
              }),
            )
            .then(() => {
              resolve(this.$ngRedux.dispatch(this.$setting.load(this.$ngRedux.getState().geoposition.city)));
            })
            .catch(reject);
        });
      });
    };
  }

  add2History(p) {
    // return {
    //   type:
    // };
  }
}
AppActions.$inject = ['$api', '$ngRedux', '$q', '$setting', '$geoposition', '$dictionary', '$state'];

export default angular.module('AppActions', []).service('$appActions', AppActions).name;
