import angular from 'angular';
import CARS from '../constants/carlist';

class CarListActions {
  constructor($api, $ngRedux, $q, $setting) {
    this._$http = $api;
    this.$ngRedux = $ngRedux;
    this._$q = $q;
  }

  carsRequest() {
    return {
      type: CARS.REQUEST,
      payload: {isRequesting: true, data: []},
    };
  }

  carsSuccess(cars) {
    return {
      type: CARS.SUCCESS,
      payload: {data: cars, isRequesting: false},
    };
  }

  carsError(error) {
    return {
      type: CARS.ERROR,
      payload: {data: error, isRequesting: false},
      erorr: true,
    };
  }

  loadCars(params) {
    return () => {
      return this._$http
        .fetch('search', params)
        .then(cars => {
          this.$ngRedux.dispatch(this.carsSuccess(cars));
        })
        .catch(err => {
          return this.$ngRedux.dispatch(this.carsError(err));
        });
    };
  }
}

CarListActions.$inject = ['$api', '$ngRedux', '$q', '$setting'];

export default angular.module('CarListActions', []).service('$carsList', CarListActions).name;
