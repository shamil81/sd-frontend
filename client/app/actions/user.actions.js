import angular from 'angular';
import USER from '../constants/user';
class UserActions {
  constructor($ngRedux, $auth, $api, $q) {
    this.$ngRedux = $ngRedux;
    this.$auth = $auth;
    this.$api = $api;
    this.$q = $q;

  }

  loginSuccess(payload) {
    return {
      type: USER.LOGIN_SUCCESS,
      payload,
    };
  }

  registrationSuccess(payload) {
    return {
      type: USER.REGISTRATION_SUCCESS,
      payload,
    };
  }

  loginError(payload) {
    return {
      type: USER.LOGIN_ERROR,
      payload,
    };
  }

  userConfirmed(payload) {
    return {
      type: USER.CONFIRM_SUCCESS,
      payload,
    };
  }

  userConfirmError(payload) {
    return {
      type: USER.CONFIRM_ERROR,
      payload,
    };
  }

  getUser() {
    if (this.$auth.isAuthenticated()) {
      return this.$api.fetch('user').then(user => {
        this.$ngRedux.dispatch(this.loginSuccess(user));
      });
    }
    else {
      this.$q.when(this.$ngRedux.dispatch(this.logout()));
    }
  }

  login(user) {
    return () => {
      this.$auth
        .login(user)
        .then(result => {
          this.$ngRedux.dispatch(this.loginSuccess(result.data.user));
        })
        .catch(err => {
          this.$ngRedux.dispatch(this.loginError(err));
        });
    };
  }

  logout() {
    return () => {
      this.$auth.logout().then(() => {
        this.$ngRedux.dispatch(
          (() => {
            return {
              type: USER.UNAUTHORIZED,
            };
          })(),
        );
      });
    };
  }

  confirmCodeSubmit(code) {
    return () => {
      this.$api
        .create('confirm', code)
        .then(user => {
          this.$ngRedux.dispatch(this.userConfirmed(user));
        })
        .catch(error => {
          this.$ngRedux.dispatch(this.userConfirmError(error));
        });
    };
  }

  register(user) {
    return () => {
      user.birthDate = this.formatDate(user.birthDate);
      user.driverLicenseDate = this.formatDate(user.driverLicenseDate);
      user.passportDate = this.formatDate(user.passportDate);

      this.$auth
        .signup(user)
        .then(result => {
          this.$auth.setToken(result);
          this.$ngRedux.dispatch(this.registrationSuccess(result.data));
        })
        .catch(console.error);
    };
  }

  formatDate(str) {
    // Assume 19810316
    // Returns 1981.03.16
    const y = str.substr(0, 4);
    const m = str.substr(4, 2);
    const d = str.substr(6, 2);
    return y + '.' + m + '.' + d;
  }
}
UserActions.$inject = ['$ngRedux', '$auth', '$api', '$q'];
export default angular.module('UserActions', []).service('$user', UserActions).name;
