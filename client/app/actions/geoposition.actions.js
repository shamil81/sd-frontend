import _ from 'lodash';
import angular from 'angular';
import GEOPOSITION from '../constants/geoposition';

class GeopositionActions {
  constructor($ngRedux, $q, $http) {
    this.position;
    this._q = $q;
    this._$ngRedux = $ngRedux;
    this._http = $http;
  }

  requestPosition() {
    return {
      type: GEOPOSITION.REQUEST,
      payload: {isRequesting: true},
    };
  }

  setGeoposition(payload, error) {
    return {
      type: GEOPOSITION.SUCCESS,
      payload,
      error,
    };
  }

  errorGeoposition(payload) {
    return {
      type: GEOPOSITION.ERROR,
      payload,
      error: true,
    };
  }

  getDefaultCity(cities) {
    let city;
    let position;

    _.forOwn(cities, v => {
      if (v.isDefault) {
        city = v.id;
        position = v.location;
        return false;
      }
      return true;
    });

    return {city, position};
  }

  get(params = {}) {
    const that = this;
    const urlParams = params.urlParams || {};
    const cities = params.cities;
    const usePositioning = params.usePositioning;
    const locale = params.locale;

    return () => {
      return this._q(function(resolve, reject) {
        const city = parseInt(urlParams.city, 10);

        // If url contains a valid city ID then return one and skip positioning
        if (city && cities[city]) {
          return resolve(that._$ngRedux.dispatch(that.setGeoposition({city: city, position: cities[city].location})));
        }

        // Set default city
        if (!usePositioning) {
          const geoposition = that.getDefaultCity(cities);

          console.dir(geoposition);

          return resolve(
            that._$ngRedux.dispatch(that.setGeoposition({city: geoposition.city, position: geoposition.position})),
          );
        }

        that._$ngRedux.dispatch(that.requestPosition());

        that
          .getPosition()
          .then(pos => {
            const position = {
              lat: pos.coords.latitude,
              lng: pos.coords.longitude,
              accuracy: pos.coords.accuracy,
            };

            that.getCity({position, cities, locale}).then(result => {
              resolve(
                that._$ngRedux.dispatch(
                  that.setGeoposition({city: result.city, place: result.place, position: result.position}),
                ),
              );
            });
          })
          .catch(err => {
            reject(that._$ngRedux.dispatch(that.errorGeoposition({isRequesting: false, errorMsg: err})));
          });
      });
    };
  }

  // Check whether a url contains lat and lng properties,
  // otherwise use browser's geolocation api
  getPosition() {
    return this._q(resolve => {
      if ('geolocation' in navigator) {
        navigator.geolocation.getCurrentPosition(resolve, resolve);

        // Firefox has a 'Not Now' button on user's geoposition permission prompt,
        // clicking this button couldn't be captured in js though.
        if (navigator.userAgent.indexOf('Firefox') > -1) {
          const timeout = setTimeout(
            () => {
              clearTimeout(timeout);

              return resolve({error: PositionError.PERMISSION_DENIED});
            },
            8000,
          );
        }
      }
      else {
        return resolve({error: 'Geoposition is not supported'});
      }
    });
  }

  // Get city ID by position coordinates, unless found return the default city
  getCity(params) {
    const that = this;
    let city;
    let place = '';
    const cities = params.cities;
    const position = params.position;
    const locale = params.locale;

    return this._q(resolve => {
      if (position && position.lat && position.lng) {
        const params = {
          latlng: position.lat + ',' + position.lng,
          language: locale,
        };

        that
          ._http({
            url: '//maps.googleapis.com/maps/api/geocode/json',
            params: params,
            method: 'get',
            skipAuthorization: true,
          })
          .then(res => {
            const area = res.data.results.filter(o => {
              return o.types.indexOf('locality') !== -1;
            })[0];

            _.forOwn(cities, v => {
              if (v.title[locale] === area.address_components[0].long_name) {
                city = v.id;
                place = res.data.results[0].formatted_address;

                return false;
              }
              return true;
            });

            if (!city) {
              city = that.getDefaultCity(cities);
            }

            resolve({city, position, place});
          })
          .catch(err => {
            console.error(err);
            resolve({city: that.getDefaultCity(cities), place});
          });
      }
    });
  }
}
GeopositionActions.$inject = ['$ngRedux', '$q', '$http'];

export default angular.module('GeopositionActions', []).service('$geoposition', GeopositionActions).name;
