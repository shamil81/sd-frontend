import angular from 'angular';
// @require "./**/*.html"

import ngMaterial from 'angular-material';
import ngRedux from 'ng-redux';
import ngReduxUiRouter from 'redux-ui-router';
import 'angular-translate';
import 'angular-translate-loader-static-files';
import 'angular-cookies';
import 'angular-translate-storage-cookie';
import 'angular-translate-storage-local';
import 'angular-dynamic-locale';
import 'angular-translate-once';
import api from './common/api';
import LoginComponent from './components/login/login';
import GeopositionActions from './actions/geoposition.actions';
import DictionaryActions from './actions/dictionary.actions';
import BookingActions from './actions/booking.actions';
import SettingActions from './actions/setting.actions';
import UserActions from './actions/user.actions';
import config from './app.config';
import AppComponent from './app.component';
import NavigationComponent from './components/navigation/navigation';
import HomeComponent from './containers/home/home';
import CarSearch from './containers/carsearch/carsearch';
import CarList from './containers/carlist/carList';
import BottomComponent from './containers/bottom/bottom';
import Page404Component from './components/page404/page404';
import BookingComponent from './containers/booking/booking';
import RegistrationComponent from './components/registration/registration';
import AppActions from './actions/app.actions';
import auth from 'satellizer';

angular
  .module('app', [
    ngRedux,
    ngReduxUiRouter,
    ngMaterial,
    LoginComponent,
    GeopositionActions,
    NavigationComponent,
    HomeComponent,
    BookingComponent,
    BottomComponent,
    SettingActions,
    UserActions,
    CarSearch,
    CarList,
    Page404Component,
    api,
    auth,
    AppActions,
    BookingActions,
    DictionaryActions,
    RegistrationComponent,
    'ngCookies',
    'pascalprecht.translate',
    'tmh.dynamicLocale',
  ])
  .config(config)
  .component('app', AppComponent);
