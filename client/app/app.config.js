import format from 'date-fns/format';
import thunk from 'redux-thunk';
import createLogger from 'redux-logger';
import {RootReducer} from './reducers';

const logger = createLogger({
  level: 'info',
  collapsed: true,
});
export default (
  $mdIconProvider,
  $locationProvider,
  $urlRouterProvider,
  $stateProvider,
  $mdDateLocaleProvider,
  $mdThemingProvider,
  $translateProvider,
  tmhDynamicLocaleProvider,
  $ngReduxProvider,
  $authProvider,
) => {
  'ngInject';
  const accentTheme = $mdThemingProvider.extendPalette('yellow', {
    A200: 'fccf3a',
    A100: 'ff0000',
    A400: '008080',
    A700: '00ff00',
  });
  $mdThemingProvider.definePalette('accentTheme', accentTheme);
  $mdThemingProvider.theme('default').primaryPalette('red').accentPalette('accentTheme');
  $mdIconProvider
    .icon('menu', 'assets/img/icons/ic_menu_24px.svg')
    .icon('passenger', 'assets/images/icons/passenger.svg')
    .icon('door', 'assets/images/icons/door.svg')
    .icon('engineVol', 'assets/images/icons/engine_vol.svg')
    .icon('luggage', 'assets/images/icons/luggage.svg')
    .icon('co2', 'assets/images/icons/arrow_right.svg')
    .icon('conditioner', 'assets/images/icons/conditioner.svg')
    .icon('conditioner_no', 'assets/images/icons/conditioner-no.svg')
    .icon('filter', 'assets/images/icons/filter.svg')
    .icon('car:coupe', 'assets/images/icons/coupe.svg')
    .icon('car:hatchback', 'assets/images/icons/hatch.svg')
    .icon('car:suv', 'assets/images/icons/crossover.svg')
    .icon('car:sedan', 'assets/images/icons/sedan.svg')
    .icon('car:wagon', 'assets/images/icons/combi.svg')
    .icon('car:allroad', 'assets/images/icons/allroad.svg')
    .icon('car:automatic', 'assets/images/icons/automatic.svg')
    .icon('car:manual', 'assets/images/icons/manual.svg')
    .icon('clock', 'assets/images/icons/clock.svg')
    .icon('calendar', 'assets/images/icons/kalend.svg')
    .icon('map:marker', 'assets/images/icons/map.svg')
    .icon('flag:ru', 'assets/images/icons/ru.svg')
    .icon('flag:en', 'assets/images/icons/gb.svg')
    .icon('user:profile', 'assets/images/icons/profile.svg')
    .icon('social:twitter', 'assets/images/icons/t.svg')
    .icon('social:vk', 'assets/images/icons/vk.svg')
    .icon('social:fb', 'assets/images/icons/f.svg');

  $locationProvider.html5Mode(true);

  $urlRouterProvider.otherwise('/ru');
  $urlRouterProvider.when('/', '/ru');

  $translateProvider.useStaticFilesLoader({
    prefix: 'assets/localization/locale-', // path to translations files
    suffix: '.json', // suffix, currently- extension of the translations
  });

  $translateProvider.preferredLanguage('ru'); // is applied on first load
  $translateProvider.useLocalStorage(); // saves selected language to localStorage
  $translateProvider.useSanitizeValueStrategy(null);
  tmhDynamicLocaleProvider.localeLocationPattern('assets/localization/angular-locale_{{locale}}.js');

  $mdDateLocaleProvider.formatDate = date => {
    return format(date, 'DD-MM-YYYY');
  };

  if (process.env.NODE_ENV == 'production') {
    $authProvider.loginUrl = '/api/auth';
    $authProvider.signupUrl = '/api/user';
    $authProvider.tokenPrefix = 'hbdfhz';
  }
  else {
    $authProvider.loginUrl = 'http://localhost:8080/api/auth';
    $authProvider.signupUrl = 'http://localhost:8080/api/user';
    $authProvider.tokenPrefix = 'hbdfh';
  }

  $authProvider.tokenType = 'JWT';

  $stateProvider
    .state('app', {
      abstract: true,
      url: '/{locale:(?:ru|en)}',

      views: {
        '': {
          template: '<app></app>',
        },
      },

      resolve: {
        // Translations
        lang: ($stateParams, $translate, tmhDynamicLocale) => {
          const loc = $stateParams.locale;
          tmhDynamicLocale.set(loc);
          return $translate.use(loc);
        },

        init: ($ngRedux, $appActions) => {
          return $ngRedux.dispatch($appActions.init());
        },

        // setting: ($ngRedux, $setting) => {
        //   console.log('resolved');
        //   return $ngRedux.dispatch($setting.load());
        // },

        // geoposition: ($stateParams, $geoposition) => {
        //   return $geoposition.getPosition($stateParams);
        // },
      },
    })
    .state('app.home', {
      url: '',
      views: {
        'main@app': {
          template: '<home></home>',
        },
        'center@app.home': {
          template: '<carsearch class="panel"></carsearch>',
        },
      },
    })
    .state('app.home.search', {
      url: '/search',
      abstract: true,
      views: {
        'carFrm@app': {
          template: '<carsearch class="panel"></carsearch>',
        },

        'carClassMenu@app': {
          template: '<carclassnav></carclassnav>',
        },
      },
    })
    .state('app.home.search.car', {
      url: '?carClass&color&carMaker&body&model&city&start&end&lat&lng&wheelDrive&fuel&isAutomatic&maxYear&minYear&maxVol&minVol',
      resolve: {
        validDates: $q => {
          return $q.when();
        },
      },
      views: {
        'main@app': {
          template: '<car-list></car-list>',
        },
      },
    })
    .state('app.login', {
      url: '/login',
      resolve: {
        init: ($auth, $state, $timeout, $q) => {
          if ($auth.isAuthenticated()) {
            $timeout(
              function() {
                $state.go('app.home');
                return $q.when();
              },
              0,
            );
          }
          return $q.when();
        },
      },
      views: {
        'main@app': {
          template: '<login class="content-container page" is-authorized="$ctrl.isAuthorized" error="$ctrl.user.error"></login>',
        },
      },
    })
    .state('app.registration', {
      url: '/registration',
      resolve: {
        init: ($auth, $state, $timeout, $q) => {
          if ($auth.isAuthenticated()) {
            $timeout(
              function() {
                $state.go('app.home');
                return $q.when();
              },
              0,
            );
          }
          return $q.when();
        },
      },
      views: {
        'main@app': {
          template: '<registration show-login-btn="false" class="content-container page" is-authorized="$ctrl.isAuthorized"></registration>',
        },
      },
    })
    .state('app.booking', {
      url: '/booking?car&start&end',
      resolve: {
        init: ($ngRedux, $stateParams, $booking, $auth) => {
          const p = angular.copy({id: $stateParams.car, start: $stateParams.start, end: $stateParams.end});
          console.dir(p);
          if ($auth.isAuthenticated()) {
            return $ngRedux.dispatch($booking.createIntent(p));
          }
          return $ngRedux.dispatch($booking.getCar(p));
        },
      },
      views: {
        'main@app': {
          template: '<booking class="content-container page booking"></booking>',
        },
      },
    })
    .state('app.profile', {
      url: '/profile',
      // resolve:{
      //   user:($user, $ngRedux) => {

      //     return $ngRedux.dispatch($user.getUser());

      //   },

      // },
      views: {
        'main@app': {
          template: '<profile class="content-container page"></profile>',
        },
      },
    })
    .state('app.page404', {
      url: '/404',
      component: 'page404',
    });

  $ngReduxProvider.createStoreWith(RootReducer, ['ngUiRouterMiddleware', thunk, logger]);
};
