import angular from 'angular';
import {stateGo, stateReload, stateTransitionTo} from 'redux-ui-router';

class AppController {
  constructor($ngRedux, tmhDynamicLocale, $user, $booking) {
    this.$ngRedux = $ngRedux;
    this.$user = $user;
    this.$booking = $booking;

    this.routerActions = {
      stateGo,
      stateReload,
      stateTransitionTo,
    };
  }

  $onInit() {
    this._unsubscribe = this.$ngRedux.connect(this.mapStateToThis.bind(this), this.routerActions)(this);

    this.$user.getUser();
  }

  login(user) {
    this.$ngRedux.dispatch(this.$user.login(user));
  }

  logout() {
    this.$ngRedux.dispatch(this.$user.logout());
  }

  register(user) {
    user.locale = this.locale;
    this.$ngRedux.dispatch(this.$user.register(user));
  }

  handlePendingBooking(state) {
    const cancel = state.booking &&
      state.booking.id &&
      state.booking.canceling !== true &&
      state.router.prevState.name === 'app.booking' &&
      state.router.currentState.name !== 'app.booking';

    if (cancel) {
      this.$ngRedux.dispatch(this.$booking.cancel());
    }
  }

  mapStateToThis(state) {
    const user = state.user;

    this.handlePendingBooking(state);

    return {
      isAuthorized: user.isAuthorized,
      user,
      locale: state.router.currentParams.locale,
    };
  }
}
AppController.$inject = ['$ngRedux', 'tmhDynamicLocale', '$user', '$booking'];
export default AppController;
