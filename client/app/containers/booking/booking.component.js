import template from './booking.html';
import controller from './booking.controller';
import './booking.less';

const bookingComponent = {
  bindings: {},
  template,
  controller,
};

export default bookingComponent;
