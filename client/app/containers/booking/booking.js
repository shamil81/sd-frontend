import angular from 'angular';
import uiRouter from 'angular-ui-router';
import bookingComponent from './booking.component';
import bookingSummary from '../../components/bookingSummary/bookingSummary';
import bookingOptions from '../../components/bookingOptions/bookingOptions';
import bookingPayment from '../../components/bookingPayment/bookingPayment';

import wizardNav from '../../common/directive/wizard-nav';
import userConfirm from '../../common/directive/user-confirm';

const bookingModule = angular
  .module('booking', [uiRouter, bookingSummary, wizardNav, bookingOptions, bookingPayment, userConfirm])
  .component('booking', bookingComponent);

export default bookingModule.name;
