import angular from 'angular';
import Container from '../container';
import _ from 'lodash';
import {stateGo} from 'redux-ui-router';

class BookingController extends Container {
  constructor($ngRedux, $booking, $translate, $mdMedia, $user, $location) {
    super($ngRedux);

    this.$ngRedux = $ngRedux;
    this.$mdMedia = $mdMedia;
    this.$user = $user;
    this.$location = $location;
    this.$booking = $booking;
    this.$translate = $translate;
    this.intentCreated;

    super.connect(this);

    let translations = [
      'selectionResult',
      'additionalOptions',
      'authorization',
      'registration',
      'mainMenu.payment',
      'confirm',
      'confirmation',
      'bookingConfirmation',
    ];

    $translate(translations).then(t => {
      this.translations = t;

      let steps = [
        {id: 1, title: t.selectionResult, name: 'page1'},
        {id: 2, title: t.additionalOptions, name: 'page2'},
        {id: 3, title: t.authorization, name: 'authorization'},
        {id: 4, title: t['mainMenu.payment'], name: 'page4'},
        {id: 5, title: t.bookingConfirmation, name: 'page5'},
      ];

      this.authUserSteps = angular.copy(steps);
      this.authUserSteps.splice(2, 1);
      this.unconfirmedUserSteps = angular.copy(this.authUserSteps);
      this.unconfirmedUserSteps.splice(2, 0, {id: 6, title: t.confirmation, name: 'page6'});
      this.unAuthUserSteps = steps;

      this.currentStep = 0;
    });
  }

  checkUser(user) {
    this.confirmErr = user.hasOwnProperty('error');
    if (user.isAuthorized === false && this.steps && this.currentStep === this.steps.length - 1) {
      this.$location.url('/' + this.locale);
    }

    if (user.isAuthorized && user.data.confirmed === false) {
      this.steps = this.unconfirmedUserSteps;
    }
    else if (user.isAuthorized && user.data.confirmed === true) {
      if (this.booking && this.booking.car && !this.booking.id && !this.intentCreated) {
        this.intentCreated = true;
        this.$ngRedux.dispatch(
          this.$booking.createIntent({id: this.booking.car.id, start: this.booking.start, end: this.booking.end}),
        );
      }
      this.steps = this.authUserSteps;
    }
    else {
      this.steps = this.unAuthUserSteps;
    }
  }

  next() {
    this.currentStep += 1;
  }

  optionSelect(item) {
    const option = item.item;
    if (option.checked) {
      this.$ngRedux.dispatch(this.$booking.addPriceOption(option));
    }
    else {
      this.$ngRedux.dispatch(this.$booking.removePriceOption(option));
    }
  }

  swap(name, translation) {
    const copy = angular.copy(this.steps);

    copy[_.findIndex(this.steps, {name: name})].title = translation;

    return copy;
  }

  onRegClick() {
    this.steps = this.swap('authorization', this.translations.registration);
    this.reg = true;
  }

  login() {
    this.steps = this.swap('authorization', this.translations.authorization);
    this.reg = false;
  }

  onCodeSubmit(hash) {
    this.$ngRedux.dispatch(this.$user.confirmCodeSubmit(hash));
  }

  makePayment() {
    this.$ngRedux.dispatch(this.$booking.create());
    this.next();
  }

  mapStateToThis(state) {
    const user = state.user;

    this.checkUser(user);

    return {
      carId: state.router.currentParams.car,
      booking: state.booking,
      locale: state.router.currentParams.locale,
      priceOptions: state.booking.priceoptions,
      user,
      carNotAvailable: state.booking.error && state.booking.error.status === 404,
    };
  }
}
BookingController.$inject = ['$ngRedux', '$booking', '$translate', '$mdMedia', '$user', '$location'];
export default BookingController;
