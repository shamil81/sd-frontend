import Container from '../container';
import {differenceInDays, addDays} from 'date-fns';

class PickdropController extends Container {
  constructor($ngRedux, $booking, $setting) {
    super($ngRedux);
    this.$ngRedux = $ngRedux;
    this.$booking = $booking;
    this.$setting = $setting;
    super.connect(this);
  }

  $onInit() {
    const start = +this._urlParams.start;
    const end = +this._urlParams.end;
    let validStart;
    let validEnd;
    let validDiff;

    console.dir(this._urlParams);

    if (start && end) {
      validDiff = differenceInDays(end, start) >= this.setting.minDiff;
    }

    if (validDiff) {
      // TODO Make this on server as well
      validStart = start && start >= this.setting.minStart && start <= this.setting.maxStart;
      validEnd = end && end >= this.setting.minEnd && end <= this.setting.maxEnd;
    }

    if (validStart && validEnd) {
      this.$ngRedux.dispatch(this.$setting.setSetting({minEnd: addDays(new Date(start), this.setting.minDiff)}));

      this.$ngRedux.dispatch(this.$booking.setDates({start: start, end: end}));
    }
    else {
      // Set defaults
      this.$ngRedux.dispatch(this.$booking.setStart({start: this.setting.minStart}));
      this.$ngRedux.dispatch(this.$booking.setEnd({end: this.setting.minEnd}));
    }
  }

  setStart(date) {
    const diff = differenceInDays(this.booking.end, date.start);

    const end = addDays(date.start, this.setting.minDiff);

    this.$ngRedux.dispatch(this.$booking.setStart(date));

    // Shift the end date if needed
    if (diff < this.setting.minDiff) {
      this.setEnd({end});
    }
    this.$ngRedux.dispatch(this.$setting.setSetting({minEnd: end}));
  }

  setEnd(date) {
    this.$ngRedux.dispatch(this.$booking.setEnd(date));
  }

  mapStateToThis(state) {
    return {
      _urlParams: state.router.currentParams,
      setting: state.setting,
      locale: state.router.currentParams.locale,
      booking: state.booking,
    };
  }
}

PickdropController.$inject = ['$ngRedux', '$booking', '$setting'];

export default PickdropController;
