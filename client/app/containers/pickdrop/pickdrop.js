import angular from 'angular';
import uiRouter from 'angular-ui-router';
import dateTimePicker from '../../components/datetimepicker/datetimepicker';

import pickdropComponent from './pickdrop.component';

const pickdropModule = angular.module('pickdrop', [uiRouter, dateTimePicker]).component('pickDrop', pickdropComponent);

export default pickdropModule.name;
