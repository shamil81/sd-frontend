import PickdropModule from './pickdrop'
import PickdropController from './pickdrop.controller';
import PickdropComponent from './pickdrop.component';
import PickdropTemplate from './pickdrop.html';

describe('Pickdrop', () => {
  let $rootScope, makeController;

  beforeEach(window.module(PickdropModule.name));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new PickdropController();
    };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    // controller specs
    it('has a name property [REMOVE]', () => { // erase if removing this.name from the controller
      let controller = makeController();
      expect(controller).to.have.property('name');
    });
  });

  describe('Template', () => {
    // template specs
    // tip: use regex to ensure correct bindings are used e.g., {{  }}
    it('has name in template [REMOVE]', () => {
      expect(PickdropTemplate).to.match(/{{\s?vm\.name\s?}}/g);
    });
  });

  describe('Component', () => {
      // component/directive specs
      let component = PickdropComponent;

      it('includes the intended template',() => {
        expect(component.template).to.equal(PickdropTemplate);
      });

      it('uses `controllerAs` syntax', () => {
        expect(component).to.have.property('controllerAs');
      });

      it('invokes the right controller', () => {
        expect(component.controller).to.equal(PickdropController);
      });
  });
});
