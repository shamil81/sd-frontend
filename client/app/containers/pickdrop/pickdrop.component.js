import template from './pickdrop.html';
import controller from './pickdrop.controller';
import './pickdrop.less';

const pickdropComponent = {
  bindings: {
    disabled: '@',
  },
  template,
  controller,
  transclude: true,
};

export default pickdropComponent;
