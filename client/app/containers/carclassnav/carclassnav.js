import angular from 'angular';
import uiRouter from 'angular-ui-router';
import carclassnavComponent from './carclassnav.component';

const carclassnavModule = angular.module('carclassnav', [
  uiRouter,
])

.component('carclassnav', carclassnavComponent);

export default carclassnavModule.name;
