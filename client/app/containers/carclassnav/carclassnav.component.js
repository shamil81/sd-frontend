import template from './carclassnav.html';
import controller from './carclassnav.controller';
import './carclassnav.less';

const carclassnavComponent = {
  bindings: {},
  template,
  controller,
};

export default carclassnavComponent;
