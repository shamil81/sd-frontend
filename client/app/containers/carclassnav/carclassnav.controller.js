import angular from 'angular';
import Container from '../container';
import {stateGo} from 'redux-ui-router';
class CarClassNavController extends Container {
  constructor($ngRedux, $timeout) {
    super($ngRedux);

    this._$ngRedux = $ngRedux;
    this._$timeout = $timeout;
  }

  $onInit() {
    super.connect(this);
  }

  carClassChange(carClass) {
    if (carClass === 0) {
      // Remove carClass
      carClass = null;
    }

    this._$ngRedux.dispatch(stateGo('app.home.search.car', {carClass}));
  }

  getCarClass(id, carClasses) {
    const cls = carClasses[id];

    if (cls) {
      return cls.title.en;
    }
    return 'all';
  }

  mapStateToThis(state) {
    const locale = state.router.currentParams.locale;
    const carClasses = state.dictionary.carclass;
    const selected = this.getCarClass(state.router.currentParams.carClass, carClasses);

    return {
      selected,
      carClasses,
      locale,
    };
  }
}
CarClassNavController.$inject = ['$ngRedux', '$timeout'];
export default CarClassNavController;
