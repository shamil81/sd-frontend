import angular from 'angular';
const carFilters = angular.module('carFilters', [])
  .filter('transmission', () => {
    return car => {
      return car.isAutomatic ? 'automatic' : 'manual';
    };
  })
  .filter('priceTile', () => {
    return car => {

      return car.pricePlan.price[Object.keys(car.pricePlan.price)[0]].limited;

    };
  })
  .filter('carTitle', () => {
    return car => {
      return car.model.maker.title + ' ' + car.model.title;

    };

  });

export default carFilters.name;
