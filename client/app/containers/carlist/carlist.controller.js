import Container from '../container';
import _ from 'lodash';
import {stateGo, stateReload, stateTransitionTo} from 'redux-ui-router';

import {chunk} from 'lodash';

class CarlistController extends Container {
  constructor($ngRedux, $carsList, $window, $booking, $appActions) {
    super($ngRedux);
    this._$carsList = $carsList;
    this.actions = {
      stateGo,
      stateReload,
      stateTransitionTo,
    };
    this.$ngRedux = $ngRedux;

    this.$booking = $booking;

    this.$appActions = $appActions;

    this.list = [];

    this.listChunks = [];

    this.loaded = 1;
    console.log('inti list');
    this.totalChunks = 0;
    this.$window = $window;

    const wh = this.$window.innerHeight;
    const ww = this.$window.innerWidth;
    this.itemsPerScroll = Math.ceil(wh / 280) * Math.ceil(ww / 280) * 0.5;

    // Currently active tile
    this._open = {};

    // Valid query params
    this._valid = [
      'skip',
      'limit',
      'lat',
      'lng',
      'city',
      'start',
      'end',
      'carClass',
      'color',
      'carMaker',
      'body',
      'model',
      'wheelDrive',
      'fuel',
      'isAutomatic',
      'maxYear',
      'minYear',
      'maxVol',
      'minVol',
    ];
  }

  loadMore() {
    console.log('loadMore');

    const chunks = this.listChunks[this.loaded];

    if (chunks) {
      chunks.forEach(car => {
        this.list.push(car);
      });

      this.loaded = this.loaded + 1;
    }
    else {
      this.scrollDisabled = true;
    }
  }

  $onInit() {
    super.connect(this);

    this.$ngRedux.dispatch(this._$carsList.loadCars(this.urlParams));
  }

  update(query) {
    query.params.locale = this.locale;

    this.$ngRedux.dispatch(stateGo('app.home.search.car', query.params, {inherit: false, reload: false}));
  }

  handleFlip(tile) {
    if (this._open.method === tile.method) {
      this._open.scope.flipOut();
    }

    tile.scope[tile.method]();

    this._open = tile;
  }

  createUrlParams(stateParams) {
    const o = {};

    this._valid.forEach(key => {
      const val = stateParams[key];

      if (val) {
        o[key] = val;
      }
    });
    return angular.copy(o);
  }

  book(car) {
    const params = angular.merge({car: car.id}, _.pick(this.urlParams, ['start', 'end']));
    // this.$ngRedux.dispatch(this.$appActions.
    this.$ngRedux.dispatch(stateGo('app.booking', params));
  }

  $onDestroy() {
    this._unsubscribe();
  }

  mapStateToThis(state) {
    const router = state.router.currentParams;
    const urlParams = this.createUrlParams(router);
    const cars = state.cars.data || [];
    const isRequesting = state.cars.isRequesting;
    // On list update scroll to top
    document.body.scrollTop = document.documentElement.scrollTop = 0;

    this.listChunks = chunk(cars, this.itemsPerScroll);
    // WRONG this updates more than once
    console.log('update');

    this.totalChunks = this.listChunks.length;
    // Init first chunk of cars
    if (this.totalChunks > 0) {
      this.scrollDisabled = false;

      this.list = this.listChunks[0];
    }
    else {
      this.list = [];
      this.scrollDisabled = true;
    }

    return {
      urlParams,
      cars,
      locale: router.locale,
      dictionary: state.dictionary,
      carClasses: state.dictionary.carclass,
      isRequesting,
    };
  }
}
CarlistController.$inject = ['$ngRedux', '$carsList', '$window', '$booking', '$appActions'];
export default CarlistController;
