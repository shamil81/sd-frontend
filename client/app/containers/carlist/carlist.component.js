import template from './carList.html';
import controller from './carList.controller';

const carlistComponent = {
  bindings: {},
  template,
  controller,
};

export default carlistComponent;
