import angular from 'angular';
import '../../common/styles/unicode-symbols.less';
import './carTile.less';
const carTile = angular.module('carTile', []).directive('carTile', () => {
  return {
    require: ['^carList', '^ngModel'],
    scope: {
      car: '=ngModel',
      locale: '=',
      q: '=query',
      onFilterApply: '&',
    },
    template: require('./carTile.tpl.html'),
    link: (scope, el, attrs, ctrls) => {
      const carListCtrl = ctrls[0];
      const query = {
        // Keys of params to remove from the url query
        discard: [],

        // Params to append to the url query
        add: {},
      };
      const prev = el.on('$destroy', () => {
        console.log('destroyed');
      });

      scope.flipBack = () => {
        carListCtrl.handleFlip({scope: scope, method: 'flipIn'});
      };

      scope.flip = () => {
        carListCtrl.handleFlip({scope: scope, method: 'flipOut'});
      };

      scope.flipIn = () => {
        scope.$broadcast('FLIP_EVENT_IN');
      };

      scope.flipOut = () => {
        console.log('flip out');

        // User either clicked the Cancel button or flipped in another tile
        // cancel unsaved filters then
        query.discard.forEach(filter => {
          angular.element(filter.id).toggleClass('sd-selected');
        });

        query.discard = [];

        Object.keys(query.add).forEach(filter => {
          const id = filter + scope.car.id;
          angular.element(document.getElementById(id)).toggleClass('sd-selected');
        });

        query.add = {};

        scope.$broadcast('FLIP_EVENT_OUT');
      };

      scope.booking = () => {
        console.dir(scope.car);
        carListCtrl.book(scope.car);
      };

      scope.onClick = (evt, data) => {
        const filter = angular.element(evt.currentTarget);
        const key = Object.keys(data)[0];

        if (filter.hasClass('sd-selected')) {
          query.discard.push({id: filter[0], key: key});
        }
        else {
          angular.extend(query.add, {[key]: data[key]});
        }

        filter.toggleClass('sd-selected');
      };

      scope.applyFilter = () => {
        query.discard.forEach(filter => {
          delete scope.q[filter.key];
        });
        angular.extend(scope.q, query.add);
        scope.onFilterApply({params: scope.q});
      };
    },
  };
});

export default carTile.name;
