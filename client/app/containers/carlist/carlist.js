import './carList.less';
import angular from 'angular';
import uiRouter from 'angular-ui-router';
import 'angular-flippy';
import carlistComponent from './carList.component';
import carclassnav from '../carclassnav/carclassnav';
import carListActions from '../../actions/carlist.actions';
import carTile from './carTile.directive';
import carFilters from './carFilters.filter';
import groupDigits from '../../common/filter/groupDigits.filter';
import ngInfiniteScroll from 'ng-infinite-scroll';
const carlistModule = angular
  .module('carList', [
    uiRouter,
    carclassnav,
    carListActions,
    carTile,
    carFilters,
    groupDigits,
    ngInfiniteScroll,
    'angular-flippy',
  ])
  .component('carList', carlistComponent);
angular.module('infinite-scroll').value('THROTTLE_MILLISECONDS', 2000);

export default carlistModule.name;
