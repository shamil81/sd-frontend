import {stateGo} from 'redux-ui-router';
import {differenceInDays, addDays} from 'date-fns';

class CarsearchController {
  constructor($ngRedux, $dictionary, $geoposition, $setting, $booking) {
    this.$ngRedux = $ngRedux;
    this.$geoposition = $geoposition;
    this.$setting = $setting;
    this.$booking = $booking;
  }

  $onInit() {
    this._unsubscribe = this.$ngRedux.connect(this.mapStateToThis.bind(this))(this);
  }

  getPosition() {
    this.$ngRedux.dispatch(this.$geoposition.get({usePositioning: true, cities: this.cities, locale: this.locale}));
  }

  setPosition(position) {
    this.$ngRedux.dispatch(
      this.$geoposition.setGeoposition({position: position.position, city: this.city, place: position.place}),
    );
  }

  cityChange(city) {
    const position = this.cities[city].location;
    this.$ngRedux.dispatch(this.$geoposition.setGeoposition({city, position}));
    this.$ngRedux.dispatch(this.$setting.load(city));
  }

  search(param) {
    this.$ngRedux.dispatch(stateGo('app.home.search.car', param, {reload: false}));
  }

  mapStateToThis(state) {
    return {
      _urlParams: state.router.currentParams,
      city: state.geoposition.city,
      cities: state.dictionary.city,
      locale: state.router.currentParams.locale,
      booking: state.booking,
      setting: state.setting,
      geoposition: state.geoposition,
      place: state.geoposition.place,
    };
  }

  $onDestroy() {
    this._unsubscribe();
  }
}
CarsearchController.$inject = ['$ngRedux', '$dictionary', '$geoposition', '$setting', '$booking'];
export default CarsearchController;
