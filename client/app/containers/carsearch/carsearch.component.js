import 'sc-date-time/sc-date-time.css';
import './carsearch.less';

import template from './carsearch.html';
import controller from './carsearch.controller';

const carsearchComponent = {

  template,
  controller,
};

export default carsearchComponent;
