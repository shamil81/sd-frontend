import angular from 'angular';
import uiRouter from 'angular-ui-router';
import googleplace from './googleplace.directive';
import blink from '../../common/directive/blink';
import setting from '../../actions/setting.actions';
import pickdrop from '../pickdrop/pickdrop';

import carsearchComponent from './carsearch.component';

const carsearchModule = angular
  .module('carsearch', [uiRouter, googleplace, blink, setting, pickdrop])
  .component('carsearch', carsearchComponent);

export default carsearchModule.name;
