
import angular from 'angular';
export default angular.module('googleplace', [])
  .directive('googleplace', () => {
    return {

      scope: {
        geoposition: '=',
        placeholder: '@',
        setPosition: '&',
      },

      link: (scope, element) => {
        const options = {
          types: [],
          componentRestrictions: { country: 'ru' },
        };

        let text = element.val() || '';

        const el = element[0];

        scope.gPlace = new google.maps.places.Autocomplete(el, options);

        scope.$watchCollection('geoposition', (geoposition) => {

          if (geoposition && geoposition.position) {
            const position = geoposition.position;
            const circle = new google.maps.Circle({
              center: { lat: parseFloat(position.lat), lng: parseFloat(position.lng) },
              radius: position.accuracy || 49,
            });

            scope.gPlace.setBounds(circle.getBounds());

            element.val(geoposition.place);

          }

        });

        const placeChangedEvt = google.maps.event.addListener(scope.gPlace, 'place_changed', () => {

          const place = scope.gPlace.getPlace();
          text = element.val();

          if (!place.geometry) {
            return scope.$apply(() => {
              element.val('');
            });
          }

          const location = { lat: place.geometry.location.lat(), lng: place.geometry.location.lng() };

          scope.setPosition({ position: location, place: text});

          scope.$apply();

        });

        scope.$on('$destroy', () => {
          google.maps.event.removeListener(placeChangedEvt);
        });

      },
    };
  }).name;
