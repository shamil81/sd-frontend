import angular from 'angular';
import uiRouter from 'angular-ui-router';
import bottomComponent from './bottom.component';

const bottomModule = angular.module('bottom', [
  uiRouter,
])

.component('bottom', bottomComponent);

export default bottomModule.name;
