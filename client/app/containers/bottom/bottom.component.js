import template from './bottom.html';
import controller from './bottom.controller';
import './bottom.less';

const bottomComponent = {
  bindings: {},
  template,
  controller,
};

export default bottomComponent;
