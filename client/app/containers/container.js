// Base Container class
class Container {
  constructor($ngRedux) {
    this._$ngRedux = $ngRedux;
  }

  connect(component) {
    component._unsubscribe = component._$ngRedux.connect(component.mapStateToThis.bind(component), component.actions)(
      component,
    );
  }
}

export default Container;
