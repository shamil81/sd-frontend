import template from './home.html';
import controller from './home.controller';
import './home.less';

const homeComponent = {
  bindings: {},
  template,
  controller,
};

export default homeComponent;
