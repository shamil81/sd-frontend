export default {
  REQUEST: 'CAR_LIST_REQUEST',
  SUCCESS: 'CAR_LIST_SUCCESS',
  ERROR: 'CAR_LIST_ERROR',
}