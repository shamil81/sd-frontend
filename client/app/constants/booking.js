export default {
  SET_START: 'SET_BOOKING_START_TIME',
  SET_END: 'SET_BOOKING_END_TIME',
  SET_CAR: 'SET_BOOKING_CAR',
  SET_DATES: 'SET_BOOKING_DATES',
  GET_CAR: 'GET_BOOKING_CAR',
  CAR_REQUEST: 'BOOKING_CAR_REQUEST',
  CAR_SUCCESS: 'BOOKING_CAR_SUCCESS',
  CAR_REQUEST_ERROR: 'BOOKING_CAR_REQUEST_ERROR',
  IMAGES: 'BOOKING_GET_CAR_IMAGES',
  PRICE_OPTIONS: 'BOOKING_PRICE_OPTIONS',
  ADD_PRICE_OPTION: 'BOOKING_ADD_PRICE_OPTION',
  REMOVE_PRICE_OPTION: 'BOOKING_REMOVE_PRICE_OPTION',
  SUCCESS: 'BOOKING_CREATED',
  INTENT_SUCCESS: 'BOOKING_INTENT_SUCCESS',
  INTENT_ERROR: 'BOOKING_INTENT_ERROR',
  CANCEL: 'BOOKING_CANCEL',
  CANCEL_REQUEST: 'BOOKING_CANCEL_REQUEST',
  CANCEL_ERROR: 'BOOKING_CANCEL_ERROR',
  ERROR: 'BOOKING_ERROR',
};
