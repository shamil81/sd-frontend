import angular from 'angular';
const userConfirm = angular.module('userConfirm', []).directive('userConfirm', $translate => {
  return {
    restrict: 'AE',

    template: require('./material/template.html'),

    scope: {
      submit: '&',
      error: '=',
      errorMsg: '@',
    },

    controller: ($scope, $element, $attrs) => {
      $translate(['code', 'requiredField', 'badCode']).then(translations => {
        $scope.fields = [
          {
            key: 'hash',
            type: 'input',
            ngModelAttrs: {
              mdAutofocus: {
                attribute: 'md-autofocus',
              },
              mdNoAsterisk: {
                attribute: 'md-no-asterisk',
              },
            },
            templateOptions: {
              type: 'text',
              label: translations.code,
              required: true,
              mdAutofocus: '',
              mdNoAsterisk: '',
            },

            validation: {
              messages: {
                required: () => {
                  return translations.requiredField;
                },

                email: () => {
                  return translations.badCode;
                },
              },
            },
          },
        ];
      });

      $scope.submitCode = () => {
        console.log($scope.hash);
        $scope.submit($scope.hash);
      };
    },
  };
});

export default userConfirm.name;
