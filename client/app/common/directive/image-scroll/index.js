import './material/style.less';
import angular from 'angular';

const imageScroll = angular.module('imageScroll', []).directive('imageScroll', () => {
  return {
    restrict: 'E',

    template: require('./material/template.html'),

    scope: {
      images: '=',
      onImageClick: '&',
      // Scroll width for desktops
      scrollWidth: '=',
    },

    link: ($scope, $element, $attrs) => {
      const isMobile = window.innerWidth < 600;
      const container = $element[0].getElementsByClassName('sb-images')[0];
      const backBtn = $element[0].getElementsByClassName('sb-back-btn')[0];
      const forwardBtn = $element[0].getElementsByClassName('sb-forward-btn')[0];
      let step;

      const imageClickHandler = e => {
        const src = e.target.src;
        $scope.$apply($scope.onImageClick({src}));
      };

      $scope.$on('destroy', () => {
        console.log('dest');
      });

      $scope.$watch('images', images => {
        backBtn.style.visibility = 'hidden';
        forwardBtn.style.visibility = 'hidden';
        if (container.children.length > 0) {
          while (container.firstChild) {
            container.removeChild(container.firstChild);
          }
        }
        if (images && images.length > 0) {
          images.forEach(image => {
            let div = document.createElement('div');
            let img = document.createElement('img');
            img.src = image.url;
            if (!isMobile) {
              img.addEventListener('click', imageClickHandler);
            }
            div.appendChild(img);

            container.appendChild(div);
          });
          step = isMobile ? container.getBoundingClientRect().width : +$scope.scrollWidth;
          backBtn.style.visibility = 'visible';
          forwardBtn.style.visibility = 'visible';
        }
      });

      $scope.scroll = dir => {
        if (dir === 'left') {
          smoothScroll(container, container.scrollLeft - step, 300);
          return;
        }
        smoothScroll(container, container.scrollLeft + step, 300);
      };

      const smoothScroll = function(element, target, duration) {
        target = Math.round(target);
        duration = Math.round(duration);
        if (duration < 0) {
          return Promise.reject('bad duration');
        }
        if (duration === 0) {
          element.scrollLeft = target;
          return Promise.resolve();
        }

        var start_time = Date.now();
        var end_time = start_time + duration;

        var start_top = element.scrollLeft;
        var distance = target - start_top;

        // based on http://en.wikipedia.org/wiki/Smoothstep
        var smooth_step = function(start, end, point) {
          if (point <= start) {
            return 0;
          }
          if (point >= end) {
            return 1;
          }
          var x = (point - start) / (end - start); // interpolation
          return x * x * (3 - 2 * x);
        };

        return new Promise(function(resolve, reject) {
          // This is to keep track of where the element's scrollLeft is
          // supposed to be, based on what we're doing
          var previous_top = element.scrollLeft;

          // This is like a think function from a game loop
          var scroll_frame = function() {
            if (element.scrollLeft != previous_top) {
              reject('interrupted');
              return;
            }

            // set the scrollLeft for this frame
            var now = Date.now();
            var point = smooth_step(start_time, end_time, now);
            var frameTop = Math.round(start_top + distance * point);
            element.scrollLeft = frameTop;

            // check if we're done!
            if (now >= end_time) {
              resolve();
              return;
            }

            // If we were supposed to scroll but didn't, then we
            // probably hit the limit, so consider it done; not
            // interrupted.
            if (element.scrollLeft === previous_top && element.scrollLeft !== frameTop) {
              resolve();
              return;
            }
            previous_top = element.scrollLeft;

            // schedule next frame for execution
            setTimeout(scroll_frame, 0);
          };

          // boostrap the animation process
          setTimeout(scroll_frame, 0);
        });
      };
    },
  };
});

export default imageScroll.name;
