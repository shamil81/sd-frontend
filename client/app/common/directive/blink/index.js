import angular from 'angular';
const blink = angular.module('blink', []).directive('blink', $timeout => {
  return {
    restrict: 'AE',
    scope: {
      blinking: '=',
    },
    controller: ($scope, $element, $attrs) => {
      $scope.speed = $attrs.speed;
      $scope.promise;
      $scope.$watch('blinking', val => {
        if (val === true) {
          showElement();
        }
        else {
          stop();
        }
      });
      function showElement() {
        $element.css('visibility', 'visible');
        $scope.promise = $timeout(hideElement, $scope.speed);
      }
      function hideElement() {
        $element.css('visibility', 'hidden');
        $scope.promise = $timeout(showElement, $scope.speed);
      }
      function stop() {
        $timeout.cancel($scope.promise);
        $element.css('visibility', 'visible');
      }
    },
  };
});

export default blink.name;
