import './material/style.less';
import angular from 'angular';
const wizardNav = angular.module('wizardNav', []).directive('wizardNav', () => {
  return {
    restrict: 'AE',

    template: require('./material/template.html'),

    scope: {
      items: '=',
      selected: '@',
    },

    controller: ($scope, $element, $attrs) => {
      const el = document.getElementsByClassName('sb-wizard-nav')[0];

      const setSelection = item => {
        const li = document.getElementById(item);
        if (li) {
          li.getElementsByTagName('span')[0].className = 'selected';
        }
      };

      $scope.$watch('selected', (val, oldVal) => {
        if (!$scope.items) {
          return;
        }

        if (oldVal) {
          const oldLi = document.getElementById($scope.items[+oldVal].name);
          if (oldLi) {
            oldLi.getElementsByTagName('span')[0].className = '';
          }
        }
        if (!val) {
          return;
        }

        setSelection($scope.items[+val].name);
      });

      $scope.$watch('items', (items, oldItems) => {
        if (items && items.length > 0) {
          if (el.children.length > 0) {
            while (el.firstChild) {
              el.removeChild(el.firstChild);
            }
          }

          items.forEach((item, i) => {
            let li = document.createElement('li');
            li.id = item.name;
            let inner = document.createElement('span');
            if (i === 0) {
              inner.style.marginLeft = 0;
            }
            let title = document.createTextNode(item.title);
            inner.appendChild(title);
            li.appendChild(inner);

            el.appendChild(li);
          });
          setSelection(items[$scope.selected].name);
        }
      });
    },
  };
});
export default wizardNav.name;
