// export const API_BASE_URL = '//192.168.1.33:8080/api/';

let API_BASE_URL = '//192.168.1.33:8080/api/';

if (process.env.NODE_ENV == 'production') {
  API_BASE_URL = '/api/';
}

export {API_BASE_URL};
