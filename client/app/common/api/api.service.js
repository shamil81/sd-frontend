import {API_BASE_URL} from './api.constants';

class APIService {
  constructor($http, $q) {
    this.http = $http;
    this.q = $q;
  }

  create(path, data) {
    return this.http
      .post(`${API_BASE_URL}${path}`, data)
      .then(response => response.data)
      .catch(response => this.q.reject(response));
  }

  destroy(path) {
    return this.http
      .delete(`${API_BASE_URL}${path}`)
      .then(response => response.data)
      .catch(response => this.q.reject(response));
  }

  fetch(path, param = {}) {
    const config = {
      url: `${API_BASE_URL}${path}`,
      method: 'GET',
      params: param,
    };
    return this.http(config).then(response => response.data).catch(response => this.q.reject(response));
  }

  update(path, data) {
    return this.http
      .put(`${API_BASE_URL}${path}`, data)
      .then(response => response.data)
      .catch(response => this.q.reject(response));
  }
}
APIService.$inject = ['$http', '$q'];
export default APIService;
