import APIService from './api.service';
export default angular.module('api', []).service('$api', APIService).name;
