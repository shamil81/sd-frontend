import USER from '../constants/user';

import {createReducer} from '../utils/utils.redux';

function loginSuccess(state, action) {
  const data = action.payload;
  return {...state, data, isAuthorized: true};
}

function loginErr(state, action) {
  const error = action.payload;
  return {...state, error, isAuthorized: false};
}

function register(state, action) {
  return {...state, data: action.payload.user, isAuthorized: true};
}

function logout(state) {
  return {...state, data: {}, isAuthorized: false};
}

function confirmSuccess(state, action) {
  return {...state, data: action.payload};
}

function confirmError(state, action) {
  return {...state, error: action.payload};
}

const reducers = {
  [USER.LOGIN_SUCCESS]: loginSuccess,
  [USER.LOGIN_ERROR]: loginErr,
  [USER.UNAUTHORIZED]: logout,
  [USER.REGISTRATION_SUCCESS]: register,
  [USER.CONFIRM_SUCCESS]: confirmSuccess,
  [USER.CONFIRM_ERROR]: confirmError,
};

export default createReducer({}, reducers);
