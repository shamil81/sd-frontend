import angular from 'angular';
import GEOPOSITION from '../constants/geoposition';

export default function setPosition(state = {}, action) {
  const payload = action.payload;

  switch (action.type) {
  case GEOPOSITION.REQUEST:
    return {...state, isRequesting: true};

  case GEOPOSITION.SUCCESS:
    return {...state, isRequesting: false, city: payload.city, position: payload.position, place: payload.place};

  case GEOPOSITION.ERROR:
    return {...state, isRequesting: false};

  default:
    return state;

  }

}

