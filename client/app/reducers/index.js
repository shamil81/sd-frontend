import {combineReducers} from 'redux';
import {router} from 'redux-ui-router';
import booking from './booking.reducer';
import dictionary from './dictionary.reducer';
import geoposition from './geoposition.reducer';
import setting from './setting.reducer';
import cars from './carlist.reducer';
import user from './user.reducer';

export const RootReducer = combineReducers({
  router,
  booking,
  dictionary,
  geoposition,
  setting,
  cars,
  user,
});

