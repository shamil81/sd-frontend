import angular from 'angular';
import SETTING from '../constants/setting';

export default function setSetting(state = {}, action){
  switch (action.type){
  case SETTING.REQUEST:
  case SETTING.SUCCESS:
    return angular.merge({}, state, action.payload);
  default:
    return state;

  }
}
