import angular from 'angular';
import CARS from '../constants/carlist';

export default function setCars(state = {}, action){
  switch (action.type) {
  case CARS.REQUEST:
    return {...state, isRequesting: true};
  case CARS.SUCCESS:
    return {...state, isRequesting: false, data: action.payload.data};
  case CARS.ERROR:
    console.log('error');
    return {...state, isRequesting: false, data: [], error: true};

  default:
    return state;
  }
}

