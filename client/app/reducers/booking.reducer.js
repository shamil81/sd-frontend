import BOOKING from '../constants/booking';
import {createReducer} from '../utils/utils.redux';

function setDates(state, action) {
  return {...state, start: action.payload.start, end: action.payload.end};
}

function setStart(state, action) {
  return {...state, start: action.payload.start};
}

function setEnd(state, action) {
  return {...state, end: action.payload.end};
}

function setCar(state, action) {
  const p = action.payload;
  console.dir(p);
  return {...state, car: {...p.car, price: p.price}, priceoptions: p.priceoptions};
}

function setPriceOptions(state, action) {
  return {...state, priceoptions: action.payload};
}
function addPriceOption(state, action) {
  let opts = state.car.options || [];
  let price = state.car.price;
  let option = action.payload;
  let newPrice = price.limited + option.price;

  return {...state, car: {...state.car, price: {...price, limited: newPrice}, options: [...opts, option]}};
}

function removePriceOption(state, action) {
  const option = action.payload;
  const stateOpts = state.car.options;
  const opts = stateOpts.map(item => {
    return item.id !== option.id;
  });

  const price = state.car.price;
  const newPrice = price.limited - option.price;

  return {...state, car: {...state.car, price: {...price, limited: newPrice}, options: opts}};
}

function onBookingSuccess(state, action) {
  return {...state, id: action.payload};
}

function onBookingIntentSuccess(state, action) {
  const p = action.payload;
  return {...state, id: p.id, error: null, car: {...p.car, price: p.price}, priceoptions: p.priceoptions};
}

function onBookingError(state, action) {
  return {...state, error: action.payload};
}

function onCancel(state, action) {
  return {...state, id: null, canceling: false};
}

function onCancelRequest(state, action) {
  return {...state, canceling: true};
}

const reducers = {
  [BOOKING.SET_START]: setStart,
  [BOOKING.SET_END]: setEnd,
  [BOOKING.SET_DATES]: setDates,
  [BOOKING.SET_CAR]: setCar,
  [BOOKING.PRICE_OPTIONS]: setPriceOptions,
  [BOOKING.ADD_PRICE_OPTION]: addPriceOption,
  [BOOKING.REMOVE_PRICE_OPTION]: removePriceOption,
  [BOOKING.INTENT_SUCCESS]: onBookingIntentSuccess,
  [BOOKING.INTENT_ERROR]: onBookingError,
  [BOOKING.SUCCESS]: onBookingSuccess,
  [BOOKING.ERROR]: onBookingError,
  [BOOKING.CANCEL]: onCancel,
  [BOOKING.CANCEL_REQUEST]: onCancelRequest,
  [BOOKING.CANCEL_ERROR]: onBookingError,
  [BOOKING.CAR_REQUEST_ERROR]: onBookingError,
};

export default createReducer({}, reducers);
