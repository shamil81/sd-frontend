import angular from 'angular';
import DICTIONARY from '../constants/dictionary';

export default function getDictionary(state = {}, action) {
  if (action.type === DICTIONARY.LOADED || action.type === DICTIONARY.ERROR) {
    return angular.merge(state, action.payload);
  }
  return state;
}
