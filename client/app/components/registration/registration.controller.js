import angular from 'angular';
import {differenceInYears, isPast} from 'date-fns';
class RegistrationController {
  constructor($translate, $ngRedux) {
    this.formReady = true;

    this.user = {};

    this.translate = $translate;

    this.$ngRedux = $ngRedux;

    this.createForm();
  }

  createForm() {
    this.translate([
      'legalEntity',
      'email',
      'badEmail',
      'password',
      'passwordLength',
      'requiredField',
      'passwordRepeat',
      'passwordMismatch',
      'license.serial',
      'badDriverLicense',
      'license.issuedDate',
      'wrongDate',
      'phone',
      'badPhone',
      'companyName',
      'inn',
      'minTen',
      'kpp',
      'address',
      'firstName',
      'badFirstName',
      'middleName',
      'lastName',
      'badLastName',
      'birthDate',
      'adultRequired',
      'passport.number',
      'badPassportNum',
      'passport.issuedDate',
      'passport.issuedBy',
      'passwordLength',
      'datePlaceholder',
    ])
      .then(translations => {
        const INNRegex = /^[0-9]{10}$/;
        // TODO: kpp regexp
        const KPPRegex = INNRegex;

        const firstNameRegex = /^[A-Я]+$/i;
        const lastNameRegex = firstNameRegex;

        const adultAge = 18;
        const passportIssueAge = 14;

        const isAdult = birthDate => {
          // console.log(new Date(birthDate));
          return Math.abs(differenceInYears(new Date(birthDate), new Date())) >= adultAge;
        };

        const isPassportDateValid = (passportDate, birthDate) => {
          // console.log(birthDate, passportDate);
          return true;
          // return Math.abs(differenceInYears(new Date(birthDate), new Date(passportDate))) >= passportIssueAge;
        };

        const mask = {
          maskDefinitions: {'1': /[3-9]/},
          clearOnBlur: true,
          addDefaultPlaceholder: false,
        };

        this.fields = [
          {
            elementAttributes: {
              layout: 'row',
              'layout-xs': 'column',
            },

            fieldGroup: [
              // Legal entity
              {
                elementAttributes: {
                  flex: '100',
                },
                key: 'isLegalEntity',
                type: 'checkbox',

                templateOptions: {
                  type: 'checkbox',
                  label: translations.legalEntity,
                },
              },
            ],
          },
          {
            elementAttributes: {
              layout: 'row',
              'layout-xs': 'column',
              'layout-align': 'space-between center',
            },
            fieldGroup: [
              // Email

              {
                className: 'flex-xs-100 flex-30',

                key: 'email',
                type: 'input',

                templateOptions: {
                  type: 'email',
                  label: translations.email,
                  required: true,
                  email: true,
                },

                validation: {
                  messages: {
                    required: () => {
                      return translations.requiredField;
                    },

                    email: () => {
                      return translations.badEmail;
                    },
                  },
                },
              },

              // Password
              {
                className: 'flex-xs-100 flex-30',

                key: 'password',
                type: 'input',

                templateOptions: {
                  type: 'password',
                  label: translations.password,
                  required: true,
                  minlength: 6,
                },

                validation: {
                  messages: {
                    required: () => {
                      return translations.requiredField;
                    },
                    minlength: () => {
                      return translations.passwordLength;
                    },
                  },
                },
              },

              // Password repeat
              {
                className: 'flex-xs-100 flex-30',

                key: 'passwordRepeat',
                type: 'input',

                templateOptions: {
                  type: 'password',
                  label: translations.passwordRepeat,
                  required: true,
                },

                validators: {
                  passwordMatch: ($viewValue, $modelValue, scope) => {
                    const val = $modelValue || $viewValue;
                    if (val) {
                      return val === scope.model.password;
                    }
                  },
                },

                validation: {
                  messages: {
                    required: () => {
                      return translations.requiredField;
                    },

                    passwordMatch: () => {
                      return translations.passwordMismatch;
                    },
                  },
                },
              },
            ],
          },
          {
            elementAttributes: {
              layout: 'row',
              'layout-xs': 'column',
              'layout-align': 'space-between center',
            },
            fieldGroup: [
              // First name
              {
                className: 'flex-xs-100 flex-30',

                key: 'firstName',
                type: 'input',

                templateOptions: {
                  type: 'input',
                  label: translations.firstName,
                  required: true,
                  pattern: firstNameRegex,
                },

                hideExpression: 'model.isLegalEntity',

                validation: {
                  messages: {
                    required: () => {
                      return translations.requiredField;
                    },
                    pattern: () => {
                      return translations.badFirstName;
                    },
                  },
                },
              },

              // Middle name

              {
                className: 'flex-xs-100 flex-30',

                key: 'middleName',
                type: 'input',

                templateOptions: {
                  type: 'input',
                  label: translations.middleName,
                },

                hideExpression: 'model.isLegalEntity',
              },

              // Last name
              {
                className: 'flex-xs-100 flex-30',

                key: 'lastName',
                type: 'input',

                templateOptions: {
                  type: 'input',
                  label: translations.lastName,
                  required: true,
                  pattern: lastNameRegex,
                },

                hideExpression: 'model.isLegalEntity',

                validation: {
                  messages: {
                    required: () => {
                      return translations.requiredField;
                    },
                    pattern: () => {
                      return translations.badLastName;
                    },
                  },
                },
              },
            ],
          },

          {
            elementAttributes: {
              layout: 'row',
              'layout-xs': 'column',
              'layout-align': 'space-between center',
            },
            fieldGroup: [
              // Birth date
              {
                className: 'flex-xs-100 flex-30',

                key: 'birthDate',
                type: 'input',

                ngModelAttrs: {
                  uiMask: {
                    attribute: 'ui-mask',
                  },
                  uiOptions: {
                    bound: 'ui-options',
                    attribute: 'ui-opts',
                  },
                },

                templateOptions: {
                  type: 'input',
                  label: translations.birthDate,
                  required: true,
                  uiMask: '9999.99.99',
                  uiOptions: mask,
                },

                expressionProperties: {
                  'templateOptions.uiOptions': 'true',
                },

                hideExpression: 'model.isLegalEntity',

                validators: {
                  adult: ($viewValue, $modelValue) => {
                    const val = $viewValue || $modelValue;
                    if (val) {
                      return isAdult(val);
                    }
                  },
                },

                validation: {
                  messages: {
                    required: () => {
                      return translations.requiredField;
                    },
                    pattern: () => {
                      return translations.wrongDate;
                    },

                    adult: () => {
                      return translations.adultRequired;
                    },
                  },
                },
              },

              // Driver lic num

              {
                className: 'flex-xs-100 flex-30',

                key: 'driverLicenseNumber',
                type: 'input',

                templateOptions: {
                  type: 'input',
                  label: translations['license.serial'],
                  required: true,
                  // pattern: driverLicenseRegex,
                },

                hideExpression: 'model.isLegalEntity',

                validation: {
                  messages: {
                    required: () => {
                      return translations.requiredField;
                    },

                    pattern: () => {
                      return translations.badDriverLicense;
                    },
                  },
                },
              },

              // Driver license issued date
              {
                className: 'flex-xs-100 flex-30',

                key: 'driverLicenseDate',
                type: 'input',

                ngModelAttrs: {
                  uiMask: {
                    attribute: 'ui-mask',
                  },
                  uiOptions: {
                    bound: 'ui-options',
                    attribute: 'ui-opts',
                  },
                },

                templateOptions: {
                  type: 'input',
                  label: translations['license.issuedDate'],
                  required: true,
                  uiMask: '9999.99.99',
                  uiOptions: mask,
                },
                expressionProperties: {
                  'templateOptions.uiOptions': 'true',
                },

                hideExpression: 'model.isLegalEntity',

                validators: {
                  past: ($viewValue, $modelValue) => {
                    const val = $viewValue || $modelValue;
                    return isPast(new Date(val));
                  },
                },

                validation: {
                  messages: {
                    required: () => {
                      return translations.requiredField;
                    },

                    past: () => {
                      return translations.wrongDate;
                    },
                  },
                },
              },
            ],
          },
          {
            elementAttributes: {
              layout: 'row',
              'layout-xs': 'column',
              'layout-align': 'space-between center',
            },
            fieldGroup: [
              // Legal entity name
              {
                className: 'flex-xs-100 flex-30',

                key: 'legalEntityName',
                type: 'input',

                templateOptions: {
                  type: 'input',
                  label: translations.companyName,
                  required: true,
                },

                hideExpression: '!model.isLegalEntity',

                validation: {
                  messages: {
                    required: () => {
                      return translations.requiredField;
                    },
                  },
                },
              },

              // INN
              {
                className: 'flex-xs-100 flex-30',

                key: 'legalEntityInn',
                type: 'input',

                templateOptions: {
                  type: 'number',
                  label: translations.inn,
                  required: true,
                  pattern: INNRegex,
                },

                hideExpression: '!model.isLegalEntity',

                validation: {
                  messages: {
                    required: () => {
                      return translations.requiredField;
                    },
                    pattern: () => {
                      return translations.minTen;
                    },
                  },
                },
              },

              // KPP
              {
                className: 'flex-xs-100 flex-30',

                key: 'legalEntityKpp',
                type: 'input',

                templateOptions: {
                  type: 'number',
                  label: translations.kpp,
                  required: true,
                  pattern: KPPRegex,
                },

                hideExpression: '!model.isLegalEntity',

                validation: {
                  messages: {
                    required: () => {
                      return translations.requiredField;
                    },
                    pattern: () => {
                      return translations.minTen;
                    },
                  },
                },
              },
            ],
          },

          {
            elementAttributes: {
              layout: 'row',
              'layout-xs': 'column',
              'layout-align': 'space-between center',
            },
            fieldGroup: [
              // Phone

              {
                className: 'flex-xs-100 flex-30',

                key: 'phone',
                type: 'input',

                ngModelAttrs: {
                  uiMask: {
                    attribute: 'ui-mask',
                  },
                  uiOptions: {
                    bound: 'ui-options',
                    attribute: 'ui-opts',
                  },
                },

                templateOptions: {
                  type: 'phone',
                  label: translations.phone,
                  required: true,
                  uiMask: '(199) 999-99-99',
                  uiOptions: mask,
                },

                expressionProperties: {
                  'templateOptions.uiOptions': 'true',
                },

                validation: {
                  messages: {
                    required: () => {
                      return translations.requiredField;
                    },
                  },
                },
              },
            ],
          },

          // Address
          {
            className: 'flex-30 flex-xs-100',

            key: 'address',
            type: 'input',

            templateOptions: {
              type: 'input',
              label: translations.address,
              required: true,
            },

            hideExpression: '!model.isLegalEntity',

            validation: {
              messages: {
                required: () => {
                  return translations.requiredField;
                },
              },
            },
          },

          {
            elementAttributes: {
              layout: 'row',
              'layout-xs': 'column',
              'layout-align': 'space-between center',
            },
            fieldGroup: [
              // Passport number
              {
                className: 'flex-xs-100 flex-30',

                key: 'passportNumber',
                type: 'input',

                templateOptions: {
                  type: 'input',
                  label: translations['passport.number'],
                  required: true,
                  // pattern: passportRegex,
                },

                expressionProperties: {
                  'templateOptions.uiOptions': 'true',
                },

                hideExpression: 'model.isLegalEntity',

                validation: {
                  messages: {
                    required: () => {
                      return translations.requiredField;
                    },
                  },
                },
              },

              // Passport issued date
              {
                className: 'flex-xs-100 flex-30',

                key: 'passportDate',
                type: 'input',

                ngModelAttrs: {
                  uiMask: {
                    attribute: 'ui-mask',
                  },
                  uiOptions: {
                    bound: 'ui-options',
                    attribute: 'ui-opts',
                  },
                },

                templateOptions: {
                  type: 'input',
                  label: translations['passport.issuedDate'],
                  required: true,
                  uiMask: '9999.99.99',
                  uiOptions: mask,
                },

                expressionProperties: {
                  'templateOptions.uiOptions': 'true',
                },

                hideExpression: 'model.isLegalEntity',

                validators: {
                  passportDate: ($viewValue, $modelValue, scope) => {
                    const val = $viewValue;
                    if (val) {
                      return isPassportDateValid(val, new Date(scope.model.birthDate));
                    }
                  },
                },

                validation: {
                  messages: {
                    required: () => {
                      return translations.requiredField;
                    },

                    passportDate: () => {
                      return translations.wrongDate;
                    },
                  },
                },
              },

              // Passport issuer

              {
                className: 'flex-xs-100 flex-30',

                key: 'passportIssuedBy',
                type: 'input',

                templateOptions: {
                  type: 'input',
                  label: translations['passport.issuedBy'],
                  required: true,
                },

                hideExpression: 'model.isLegalEntity',

                validation: {
                  messages: {
                    required: () => {
                      return translations.requiredField;
                    },
                  },
                },
              },
            ],
          },
        ];

        // this.formReady = true;
      })
      .catch(console.error);
  }

  onLogin() {
    this.login();
  }

  
  

  submit(user) {
    if (this.form.$invalid) {
      return;
    }

    const userCopy = angular.copy(user);

    delete userCopy.passwordRepeat;

    this.app.register(userCopy);
  }
}
RegistrationController.$inject = ['$translate', '$ngRedux'];
export default RegistrationController;
