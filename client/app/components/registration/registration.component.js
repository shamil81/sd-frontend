import template from './registration.html';
import controller from './registration.controller';
import './registration.less';

const registrationComponent = {
  bindings: {
    onSuccess: '&',
    login: '&',
    showLoginBtn: '<',
    isAuthorized: '<',
  },
  template,
  controller,
  require: {
    app: '^app',
  },
};

export default registrationComponent;
