import angular from 'angular';
import uiRouter from 'angular-ui-router';
import registrationComponent from './registration.component';
import uiMask from 'angular-ui-mask';

const registrationModule = angular
  .module('registration', [uiRouter, uiMask])
  .component('registration', registrationComponent);

export default registrationModule.name;
