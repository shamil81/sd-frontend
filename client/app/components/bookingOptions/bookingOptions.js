import angular from 'angular';
import uiRouter from 'angular-ui-router';
import bookingOptionsComponent from './bookingOptions.component';

const bookingOptionsModule = angular.module('bookingOptions', [
  uiRouter,
])

.component('bookingOptions', bookingOptionsComponent);

export default bookingOptionsModule.name;
