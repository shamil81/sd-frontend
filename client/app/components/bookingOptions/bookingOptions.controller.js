class BookingOptionsController {
  constructor() {
    this.opts = [];
    this.loc;
  }

  $onChanges(changes) {
    let val;
    if (changes.locale) {
      this.loc = changes.locale.currentValue;
    }

    if (changes.options) {
      val = changes.options.currentValue;
    }

    if (changes.booking) {
      this.price = changes.booking.currentValue.car.price;
    }

    if (val) {
      let ar = [];
      val.forEach(item => {
        const o = {title: item.title[this.loc], id: item.id, price: item.price, checked: false};
        this.opts.push(o);
      });
    }
  }

  onClick(item) {
    this.optionSelect({item});
  }
}
export default BookingOptionsController;
