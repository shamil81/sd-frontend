import template from './bookingOptions.html';
import controller from './bookingOptions.controller';
import './bookingOptions.less';

const bookingOptionsComponent = {
  bindings: {
    booking: '<',
    options: '<',
    locale: '<',
    optionSelect: '&',
    next: '&',
  },
  template,
  controller,
};

export default bookingOptionsComponent;
