import template from './page404.html';
import controller from './page404.controller';
import './page404.less';

const page404Component = {
  bindings: {},
  template,
  controller,
};

export default page404Component;
