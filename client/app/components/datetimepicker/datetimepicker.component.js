import template from './datetimepicker.html';
import controller from './datetimepicker.controller';

const datetimepickerComponent = {
  bindings: {
    disabled: '<',
    date: '<',
    header: '@',
    onChange: '&',
    locale: '@',
    mindate: '<',
    maxdate: '<',
  },
  template,
  controller,
};

export default datetimepickerComponent;
