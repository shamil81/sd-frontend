import DatetimepickerModule from './datetimepicker';
import DatetimepickerController from './datetimepicker.controller';
import DatetimepickerComponent from './datetimepicker.component';
import DatetimepickerTemplate from './datetimepicker.html';

describe('Datetimepicker', () => {
  let $rootScope, makeController;

  beforeEach(window.module(DatetimepickerModule.name));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new DatetimepickerController();
    };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    // controller specs
    it('has a name property [REMOVE]', () => { // erase if removing this.name from the controller
      const controller = makeController();
      expect(controller).to.have.property('name');
    });
  });

  describe('Template', () => {
    // template specs
    // tip: use regex to ensure correct bindings are used e.g., {{  }}
    it('has name in template [REMOVE]', () => {
      expect(DatetimepickerTemplate).to.match(/{{\s?vm\.name\s?}}/g);
    });
  });

  describe('Component', () => {
      // component/directive specs
    const component = DatetimepickerComponent;

    it('includes the intended template', () => {
      expect(component.template).to.equal(DatetimepickerTemplate);
    });

    it('uses `controllerAs` syntax', () => {
      expect(component).to.have.property('controllerAs');
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(DatetimepickerController);
    });
  });
});
