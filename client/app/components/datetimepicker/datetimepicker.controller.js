import ngMaterial from 'angular-material';
const dialogTpl = require('./dialog.html');

class DatetimepickerController {
  constructor($mdDialog, $scope, $element) {
    this._$mdDialog = $mdDialog;
    this.div = $element[0].children[0];
  }

  $onChanges(changes) {
    const val = changes.disabled;

    if (val && val.currentValue === 'true') {
      this.div.classList.remove('sd-link');
      this.disabled = true;
    }
    else {
      this.div.classList.add('sd-link');
      this.disabled = false;
    }
  }
  onClick() {
    let _openModal;
    const that = this;

    _openModal = this._$mdDialog
      .show({
        controller: function($scope, $mdDialog) {
          $scope.model = that.date;

          console.log($scope.model);

          console.dir(that.mindate);
          $scope.mindate = that.mindate.toDateString();
          $scope.maxdate = that.maxdate.toDateString();

          $scope.locale = that.locale;

          $scope.onCancel = function() {
            $mdDialog.cancel();
          };

          $scope.onSave = function(val) {
            $mdDialog.hide($scope.model);
          };
        },
        template: dialogTpl,
        parent: angular.element(document.body),
        clickOutsideToClose: true,
        show: true,
      })
      .then(
        function(newDate) {
          that.onChange({start: newDate, end: newDate});

          _openModal = null;
        },
        function() {
          return;
        },
      );
  }
}
DatetimepickerController.$inject = ['$mdDialog', '$scope', '$element'];
export default DatetimepickerController;
