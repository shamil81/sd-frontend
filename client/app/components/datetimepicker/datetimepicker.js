import angular from 'angular';
import uiRouter from 'angular-ui-router';
import datetimepickerComponent from './datetimepicker.component';
import 'sc-date-time/sc-date-time';
import './datetimepicker.less';
// sbu
const datetimepickerModule = angular.module('datetimepicker', [
  uiRouter,
  'scDateTime',
])
.value('scDateTimeConfig', {
  defaultTheme: 'material',
  autosave: false,
  defaultMode: 'date',
  displayMode: (window.screen.availWidth < 1024) ? undefined : 'full',
  defaultOrientation: false,
  displayTwentyfour: true,
  compact: (window.screen.availWidth < 1024), // Compact mode for devices less than 1024 screen width

})

.component('datetimePicker', datetimepickerComponent);

export default datetimepickerModule.name;
