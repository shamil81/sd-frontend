import template from './profile.html';
import controller from './profile.controller';
import './profile.less';

const profileComponent = {
  bindings: {},
  template,
  controller,
};

export default profileComponent;
