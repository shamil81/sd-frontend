class BookingPaymentController {
  constructor($filter) {
    this.$filter = $filter;
    this.frm = {
      paymentMethod: 'bankCard',
      coupon: '',
      feePercent: 2,
    };

    this.minPaymentPercent = 30;

    this.percentOfTotal = 0;
  }

  paySumChange() {
    this.percentOfTotal = parseInt(this.frm.fee * 100 / this.car.price.limited, 10) || 0;
  }

  $onChanges(changes) {
    if (changes.car) {
      const min = this.$filter('groupDigits')(parseInt(changes.car.currentValue.price.limited * 30 * 0.01, 10));

      this.translations = {
        minAmount: min,
      };
    }
  }
}
BookingPaymentController.$inject = ['$filter'];
export default BookingPaymentController;
