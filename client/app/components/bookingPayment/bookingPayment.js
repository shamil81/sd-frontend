import angular from 'angular';
import uiRouter from 'angular-ui-router';
import bookingPaymentComponent from './bookingPayment.component';
import groupDigits from '../../common/filter/groupDigits.filter';

const bookingPaymentModule = angular
  .module('bookingPayment', [uiRouter, groupDigits])
  .component('bookingPayment', bookingPaymentComponent);

export default bookingPaymentModule.name;
