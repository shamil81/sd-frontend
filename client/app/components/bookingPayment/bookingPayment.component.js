import template from './bookingPayment.html';
import controller from './bookingPayment.controller';
import './bookingPayment.less';

const bookingPaymentComponent = {
  bindings: {
    car: '<',
    onPay: '&',
  },
  template,
  controller,
};

export default bookingPaymentComponent;
