import angular from 'angular';
import uiRouter from 'angular-ui-router';
import navigationComponent from './navigation.component';
import navigationMobile from './navigation.mobile.directive';
import auth from 'satellizer';

const navigationModule = angular.module('navigation', [
  uiRouter,
  navigationMobile,
  auth,
])

.component('navigation', navigationComponent);

export default navigationModule.name;
