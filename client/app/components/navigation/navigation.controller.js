import {stateGo, stateReload, stateTransitionTo} from 'redux-ui-router';
import format from 'date-fns/format';
import ruLocale from 'date-fns/locale/ru';

class NavigationController {
  constructor($ngRedux, $user) {
    const routerActions = {
      stateGo,
      stateReload,
      stateTransitionTo,
    };

    this._unsubscribe = $ngRedux.connect(this.mapStateToThis.bind(this), routerActions)(this);
    this._$ngRedux = $ngRedux;
    this.$user = $user;
  }

  changeLocale(locale) {
    stateGo('app.home', {locale: locale}, {reload: true, inherit: false, notify: true});
  }

  _getNextLocale(locale) {
    return locale === 'ru' ? {key: 'en', title: 'English'} : {key: 'ru', title: 'Русский'};
  }
  logout() {
    this._$ngRedux.dispatch(this.$user.logout());
  }

  toggleSideNav() {
    this.sideNavOpen = !this.sideNavOpen;
  }

  mapStateToThis(state) {
    const currentlocale = state.router.currentParams.locale;

    let l;

    if (currentlocale === 'ru') {
      l = ruLocale;
    }

    const nextLocale = this._getNextLocale(currentlocale);
    const gp = state.geoposition;
    const booking = state.booking;
    const city = state.dictionary.city[gp.city].title[currentlocale] || '';

    return {
      user: state.user,
      nextLocale,
      city,
      place: gp.place || null,
      start: format(new Date(booking.start), 'ddd, HH:mm D MMMM, YYYY', {locale: l}),
      end: format(new Date(booking.end), 'ddd, HH:mm D MMMM, YYYY', {locale: l}),
    };
  }

  $onDestroy() {
    this._unsubscribe();
  }
}
NavigationController.$inject = ['$ngRedux', '$user'];
export default NavigationController;
