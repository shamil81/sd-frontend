import angular from 'angular';
import './navigation.mobile.less';

const navigationMobile = angular.module('navigationMobile', [])
  .directive('navigationMobile', () => {

    return {

      template: require('./navigation.mobile.html'),
    };

  });

export default navigationMobile.name;
