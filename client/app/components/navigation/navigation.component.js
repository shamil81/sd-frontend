import template from './navigation.html';
import controller from './navigation.controller';
import './navigation.less';

const navigationComponent = {
  template,
  controller,
};

export default navigationComponent;
