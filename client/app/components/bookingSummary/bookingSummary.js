import 'odometer/themes/odometer-theme-default.css';
import angular from 'angular';
import uiRouter from 'angular-ui-router';
import bookingSummaryComponent from './bookingSummary.component';
import pickdrop from '../../containers/pickdrop/pickdrop';
import odometer from 'odometer';
window.Odometer = odometer;
import 'angular-odometer-js';
import groupDigits from '../../common/filter/groupDigits.filter';

import imageScroll from '../../common/directive/image-scroll';

const bookingSummaryModule = angular
  .module('bookingSummary', [uiRouter, pickdrop, 'ui.odometer', imageScroll, groupDigits])
  .component('bookingSummary', bookingSummaryComponent);

export default bookingSummaryModule.name;
