import {getYear, differenceInDays, distanceInWordsStrict, isThisYear} from 'date-fns';
import ru from 'date-fns/locale/ru';
class BookingSummaryController {
  constructor($translate, $mdMedia, $window, $sce) {
    this.$translate = $translate;
    this.$mdMedia = $mdMedia;
    this.isMobile = $mdMedia('xs');
    this.window = $window;
    this.$sce = $sce;
  }

  $onChanges(changes) {
    if (!this.format && !this.loc && changes.locale) {
      const isRu = changes.locale.currentValue === 'ru';
      this.loc = isRu ? {locale: ru} : {};
    }

    if (changes.booking) {
      const booking = changes.booking.currentValue;

      this.car = booking.car;
      if (this.car) {
        const year = getYear(new Date());

        this.age = !isThisYear(new Date(this.car.year), new Date())
          ? distanceInWordsStrict(new Date(this.car.year, 0, 1), new Date(year, 0, 1), this.loc)
          : false;

        this.runLimit = differenceInDays(booking.end, booking.start) * this.car.price.runLimit;
        this.hasImages = this.car.images.length > 0;
        this.showMainImg = this.showImage();

        // Show reviews and/or about model info
        this.showAbout();
      }
    }
  }

  showAbout() {
    this.$translate(['mainMenu.reviews', 'aboutModel']).then(translations => {
      this.aboutModelTrans = translations;
      this.about = [];
      this.selected = {};
      this.selectedContent = '';
      if (this.car.reviews) {
        this.about.push({id: 1, title: this.aboutModelTrans['mainMenu.reviews'], content: this.car.reviews});
      }
      if (this.car.model.media) {
        this.about.push({id: 2, title: this.aboutModelTrans.aboutModel, content: this.car.model.media[this.locale]});
      }

      if (this.about.length > 0) {
        this.selected = this.about[0];
        this.selectedContent = this.$sce.trustAsHtml(this.selected.content);
      }
    });
  }

  reload() {
    location.reload();
  }

  $onInit() {
    this.window.addEventListener('orientationchange', this.reload);
  }

  $onDestroy() {
    this.window.removeEventListener('orientationchange', this.reload);
  }

  itemChange(id) {}

  /** Main image will be shown:
     *  1. Screen is larger than xs
     *  2. Car has no images and screen is xs
     */
  showImage() {
    if (this.hasImages) {
      this.image = this.car.images[0].url;
    }
    else {
      this.image = this.car.model.image;
    }

    return !(this.hasImages && this.isMobile);
  }

  selectImage(img) {
    this.image = img.src;
  }
}
BookingSummaryController.$inject = ['$translate', '$mdMedia', '$window', '$sce'];
export default BookingSummaryController;
