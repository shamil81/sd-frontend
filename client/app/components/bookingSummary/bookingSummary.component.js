import template from './bookingSummary.html';
import controller from './bookingSummary.controller';
import './bookingSummary.less';

const bookingSummaryComponent = {
  bindings: {
    booking: '<',
    locale: '<',
    next: '&',
  },
  template,
  controller,
};

export default bookingSummaryComponent;
