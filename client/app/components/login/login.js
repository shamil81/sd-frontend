import angular from 'angular';
import uiRouter from 'angular-ui-router';
import './login.less';
import loginComponent from './login.component';
import formly from 'angular-formly';
import 'angular-formly-material';
import 'angular-messages';
import 'angular-translate';

const loginModule = angular.module('login', [
  uiRouter,
  formly,
  'ngMessages',
  'formlyMaterial',
  'pascalprecht.translate',

])

.component('login', loginComponent);

export default loginModule.name;
