import template from './login.html';
import controller from './login.controller';

const loginComponent = {
  bindings: {
    error: '<',
    onRegistration: '&',
  },

  require: {
    app: '^app',
  },

  template,
  controller,
};

export default loginComponent;
