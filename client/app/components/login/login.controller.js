class LoginController {
  constructor($translate, $mdMenu, $rootScope) {
    this.$mdMenu = $mdMenu;
    this.formReady = false;

    $translate(['email', 'password', 'requiredField', 'badEmail'])
      .then(translations => {
        this.fields = [
          {
            key: 'email',
            type: 'input',
            ngModelAttrs: {
              mdAutofocus: {
                attribute: 'md-autofocus',
              },
              mdNoAsterisk: {
                attribute: 'md-no-asterisk',
              },
            },
            templateOptions: {
              type: 'email',
              label: translations.email,
              required: true,
              email: true,
              mdAutofocus: '',
              mdNoAsterisk: '',
            },

            validation: {
              messages: {
                required: () => {
                  return translations.requiredField;
                },

                email: () => {
                  return translations.badEmail;
                },
              },
            },
          },

          {
            key: 'password',
            type: 'input',

            ngModelAttrs: {
              mdNoAsterisk: {
                attribute: 'md-no-asterisk',
              },
            },

            templateOptions: {
              type: 'password',
              label: translations.password,
              required: true,
              mdNoAsterisk: '',
            },

            validation: {
              messages: {
                required: () => {
                  return translations.requiredField;
                },
              },
            },
          },
        ];

        this.formReady = true;
      })
      .catch(console.error);

    $rootScope.$on('$mdMenuOpen', () => {
      this.wrongCredentials = false;
      this.options.resetModel();
    });
  }

  $onChanges(changes) {
    const val = changes.error.currentValue;

    this.wrongCredentials = val && val.status === 404;
  }

  register() {
    this.onRegistration();
  }

  submit(user) {
    if (this.form.$invalid) {
      return;
    }

    console.dir(this.app);

    this.app.login(user);
  }
}
LoginController.$inject = ['$translate', '$mdMenu', '$rootScope'];
export default LoginController;
