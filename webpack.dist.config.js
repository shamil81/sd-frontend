const webpack = require('webpack');
const path = require('path');
const config = require('./webpack.config');

config.devtool = '#eval';

config.output = {
  filename: '[name].bundle.js',
  publicPath: '',
  path: path.resolve(__dirname, 'dist'),
};

config.plugins = config.plugins.concat([
  new webpack.DefinePlugin({
    'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV || 'production'),
  }),

  // Reduces bundles total size
  //   new webpack.optimize.UglifyJsPlugin({
  //     output: {
  //       comments: false,
  //     },
  //     mangle: false,
  //     compress: {
  //       pure_funcs: ['console.log'],
  //       warnings: false,
  //     },
  //     // mangle: {
  //     //   //   // You can specify all variables that should not be mangled.
  //     //   //   // For example if your vendor dependency doesn't use modules
  //     //   //   // and relies on global variables. Most of angular modules relies on
  //     //   //   // angular global variable, so we should keep it unchanged
  //     //   except: ['$super', '$', 'exports', 'require', 'angular'],
  //     // },
  //   }),
]);

module.exports = config;
